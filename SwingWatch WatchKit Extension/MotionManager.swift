/*
 Copyright (C) 2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 This class manages the CoreMotion interactions and 
         provides a delegate to indicate changes in data.
 */

import Foundation
import CoreMotion
import WatchKit

/**
 `MotionManagerDelegate` exists to inform delegates of motion changes.
 These contexts can be used to enable application specific behavior.
 */
protocol MotionManagerDelegate: class {
    func didDetectedReps(_ reps : Int)
    func didResetDetectingState()
    func didReportDetectedExercise(_ name: String)
    func showExerciseLogged()
    func notifylogExercise()
}

class MotionManager :  MotionDetectDelegate {
    // MARK: Properties
    
    let motionManager = CMMotionManager()
    let queue = OperationQueue()
    
    // MARK: Application Specific Constants
    
    // These constants were derived from data and should be further tuned for your needs.

    // The app is using 20hz data and the buffer is going to hold 1s worth of data.
    let sampleInterval = 1.0 / 20

    weak var delegate: MotionManagerDelegate?
    
    let motionDetector = MotionDetect.sharedInstance() as! MotionDetect

    // MARK: Initialization
    
    init() {
        // Serial queue for sample handling and calculations.
        queue.maxConcurrentOperationCount = 1
        queue.name = "MotionManagerQueue"
    }

    // MARK: Motion Manager
    func startUpdates() {
        if !motionManager.isDeviceMotionAvailable {
            print("Device Motion is not available.")
            return
        }

        // Reset everything when we start. ?
        motionDetector.reset()
        motionManager.deviceMotionUpdateInterval = sampleInterval
        motionManager.startDeviceMotionUpdates(to: queue) {
            (deviceMotion: CMDeviceMotion?, error:NSError?) in
            if error != nil {
                print("Encountered error: \(error!)")
            }

            if deviceMotion != nil {
                self.motionDetector.postNewSensor(deviceMotion)
            }
        }
    }

    func stopUpdates() {
        if motionManager.isDeviceMotionAvailable {
            motionManager.stopDeviceMotionUpdates()
        }
    }
    
    // MARK: MotionDetect Callback
    func setDetecting() {
        delegate?.didResetDetectingState()
    }
    
    func reportRep(_ reps:Int32, wDetector detector:Int32) {
        delegate?.didDetectedReps(Int(reps))
    }

    func reportDetectedExercise(_ name:String) {
        delegate?.didReportDetectedExercise(name)
    }
    
    func showExerciseLogged() {
        delegate?.showExerciseLogged()
    
    }   // tell user we have recorded an exercise. This function should vibrate, then bring up edit interface
    
    func logExercise() {
        delegate?.notifylogExercise()
    }  // log the exercise in SaveState to server

    var logs = ""
    func addLog(_ log: String!) {
        logs += " " + log
    }
}

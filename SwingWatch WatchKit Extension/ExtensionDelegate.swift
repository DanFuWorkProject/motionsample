/*
 Copyright (C) 2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 This class is the main entry point of the extension.
 */
import WatchKit

class ExtensionDelegate: NSObject, WKExtensionDelegate {
    
    var unit = 0
    
    func applicationDidFinishLaunching() {
        let defaults = UserDefaults(suiteName: "group.co.vimo.sample")
        if let unit = (defaults?.integer(forKey: "unit_preference")){
            self.unit = unit
        }
        
        
        
//        Parse.enableLocalDatastore()
//        
//        // ****************************************************************************
//        // Uncomment this line if you want to enable Crash Reporting
//        // ParseCrashReporting.enable()
//        //
//        // Uncomment and fill in with your Parse credentials:
//        Parse.setApplicationId("bUn36eb6NVZ5OieFQ1VgEYIaUhSIG3YUCAQnGpLk", clientKey: "IId6qoKf3leEj0RUYNOBcpYvlTiIxPc3dcWKnBoZ")
//        
//        
//        PFUser.enableAutomaticUser()
//        
//        let defaultACL = PFACL();
//        
//        // If you would like all objects to be private by default, remove this line.
//        //        defaultACL.setPublicReadAccess(true)
//        
//        PFACL.setDefault(defaultACL, withAccessForCurrentUser:true)
        

        
    }
}

//
//  ManualEntryController.swift
//  vimofit
//
//  Created by Ethan Fan on 7/15/15.
//  Copyright © 2015 Vimo Lab. All rights reserved.
//

import Foundation
import WatchKit


class NameEntryController : WKInterfaceController{
    /*
    
    @IBOutlet var table: WKInterfaceTable!
    
    let mgr = WorkoutManager.sharedInstance
    var exerciseNames = [String]()
    weak var detectionController : DetectionController? = nil
    
    // save ordered list globally, need to use it to look up weight when user chooses one
    var orderedExer : [[String : AnyObject]] = []
    
    override func awake(withContext context: AnyObject?) {
        super.awake(withContext: context)
        
        // set to detecting state immediately when log screen up
        guard let ctxObj = context as? [String : DetectionController] else {return}
        if let controller = ctxObj["controller"]{
            detectionController = controller
            controller.pendingScreenState = ScreenState.Detecting
        }
    }
    
    override func willActivate() {
        super.willActivate()
        
        exerciseNames.removeAll()
        // always refresh
        
        let motionDetect = MotionDetect.sharedInstance() as! MotionDetect
        orderedExer = motionDetect.getOrderedList()
        for exer in orderedExer{
            exerciseNames.append(exer["name"] as! String)
        }
        self.loadTableData()
        
    }
    
    
    // =========================================================================
    // MARK: Private
    
    private func loadTableData() {
        
        let exerCount = exerciseNames.count + 1 // for manual entry
        table.setNumberOfRows(exerCount, withRowType: "Cell")
        
        var i = 0
        // voice button
        let row = table.rowController(at: i) as! ManualTableRowController
        row.group.setBackgroundImageNamed("purpleButton")
        row.showItem(title: "Manual", image: UIImage(named: "mic"))
        i += 1
        for name in exerciseNames {
            let row = table.rowController(at: i) as! ManualTableRowController
            row.showItem(title: name as String, image: nil)
            i += 1
        }
        
        
        //
        //        if let selectedExercise = mgr.selectedExercise{
        //            table.setNumberOfRows(exerCount + 1, withRowType: "Cell")
        //            let row = table.rowControllerAtIndex(i) as! ManualTableRowController
        //            row.group.setBackgroundImageNamed("redButton")
        //            row.showItem(selectedExercise, image: UIImage(named: "checkmark"))
        //            i++
        //        }else{
        //            table.setNumberOfRows(exerCount, withRowType: "Cell")
        //        }
        //
        
    }
    
    // =========================================================================
    // MARK: WKInterfaceTable
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        var tableStart = 0
        
        
        if rowIndex == 0 {
            self.showManualEntry()
            
        }else{
            tableStart = 1
            let name = exerciseNames[rowIndex - tableStart] as String
            
            let motionDetect = MotionDetect.sharedInstance() as! MotionDetect
            self.mgr.motionLog.mBestMatchedExercise = name

            motionDetect.saved(self.mgr.motionLog, atReps: self.mgr.motionLog.mOriginalReps, atWeight: 0, atUnit: 0)
            self.dismiss()
            
            //todo : change screen state to detecting
        }
    }
    
    private func showManualEntry(){
        
        
        let hints = ["Bench press", "Jumping Jacks", "Squats"]
    
        self.presentTextInputController(withSuggestions: hints, allowedInputMode:
        .plain) { (results) -> Void in
            guard let results = results else {return} // there are chance of nil or no data
            
            if let selectedExercise = results.first as? String{
                let motionDetect = MotionDetect.sharedInstance() as! MotionDetect

                self.mgr.motionLog.mBestMatchedExercise = selectedExercise
                motionDetect.saved(self.mgr.motionLog, atReps: self.mgr.motionLog.mOriginalReps, atWeight: 0, atUnit: 0)
                self.dismiss()
                
                //todo : change screen state to detecting
            }
        }
    }
    // look up last used weight
    //        if let _ = mgr.selectedExercise {
    //            for exer in _orderedExer {
    //                if mgr.selectedExercise == exer["name"] as! String {
    //                    mgr.weight = exer["weight"]!.integerValue
    //                    mgr.unit = WeightUnit(rawValue: exer["weightunit"]!.integerValue)!
    //                }
    //            }
    //        }
 
 */
}

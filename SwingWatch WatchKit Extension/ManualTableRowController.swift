//
//  ManualTableRowController.swift
//  vimofit
//
//  Created by Ethan Fan on 7/19/15.
//  Copyright © 2015 Vimo Lab. All rights reserved.
//

import Foundation
import WatchKit

class ManualTableRowController: NSObject {
    
    @IBOutlet var group: WKInterfaceGroup!

    @IBOutlet var imageInterface: WKInterfaceImage!

    @IBOutlet var titleLabel: WKInterfaceLabel!
    

    func showItem(title: String, image: UIImage?) {
        self.titleLabel.setText(title)
        self.imageInterface.setImage(image)
        
    }
}


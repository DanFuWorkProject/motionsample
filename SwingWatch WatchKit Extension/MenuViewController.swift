//
//  MenuViewController.swift
//  vimofit
//
//  Created by Jesus Cagide on 10/26/15.
//  Copyright © 2015 Vimo Lab. All rights reserved.
//

import WatchKit
import Foundation


class MenuViewController: WKInterfaceController {
    
    @IBOutlet private var timerInterface: WKInterfaceTimer!

    override func awake(withContext context: AnyObject?) {
        super.awake(withContext: context)
        timerInterface.start()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }


    @IBAction func endWorkoutBtnTapped(){
        timerInterface.stop()
        
        WorkoutManager.sharedInstance.stopWorkout()
    }
    
}

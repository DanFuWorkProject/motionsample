import Foundation
import WatchKit
import WatchConnectivity
import HealthKit
import CoreMotion


class InterfaceController: WKInterfaceController {
    
    
    override func awake(withContext context: AnyObject?) {
        super.awake(withContext: context)
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    // =========================================================================
    // MARK: - Actions
    
    @IBAction func startBtnTapped() {
        WKInterfaceController.reloadRootControllers(withNames: ["menu","detect"], contexts: nil)
    }
    
}

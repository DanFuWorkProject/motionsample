//
//  LoggedInterfaceController.swift
//  SwingWatch
//
//  Created by 付 旦 on 6/16/16.
//  Copyright © 2016 Apple Inc. All rights reserved.
//

import WatchKit
import Foundation


class LoggedInterfaceController: WKInterfaceController {
    
    
    
    @IBOutlet var exerciseNameLabel: WKInterfaceLabel!
    @IBOutlet var exerciseRepAndWeightLabel: WKInterfaceLabel!
    @IBOutlet var consistencyLabel: WKInterfaceLabel!
    @IBOutlet var velocityLabel: WKInterfaceLabel!
    @IBOutlet var restingTimer: WKInterfaceTimer!

    override func awake(withContext context: AnyObject?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}

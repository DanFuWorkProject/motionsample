import Foundation
import WatchKit

class MotionRowController: NSObject {
    
    @IBOutlet var group: WKInterfaceGroup!
    
    @IBOutlet var titleLabel: WKInterfaceLabel!
    
    
    func showItem(title: String) {
        self.titleLabel.setText(title)
    }
}

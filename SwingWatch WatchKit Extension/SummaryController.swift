//
//  SummaryInterfaceController.swift
//  SwingWatch
//
//  Created by Ethan Fan on 6/21/16.
//  Copyright © 2016 Apple Inc. All rights reserved.
//

import WatchKit
import Foundation


class SummaryController: WKInterfaceController {
    
    static let kTitleLogged = "Logged"
    static let kTitleCreateName = "Give a name"
    static let kNoNamePlaceholder = "???"
    
    @IBOutlet var repsPicker: WKInterfacePicker!
    @IBOutlet var weightPicker: WKInterfacePicker!
    @IBOutlet var titleLabel: WKInterfaceLabel!
    
    @IBOutlet var nameLabel: WKInterfaceLabel!
    @IBOutlet var consistencyLabel: WKInterfaceLabel!
    @IBOutlet var velocityLabel: WKInterfaceLabel!
    @IBOutlet var heartRateChart: WKInterfaceImage!
    
    let mgr = WorkoutManager.sharedInstance
    weak var detectionController : DetectionController?

    var autoDismiss = false
    
    override func awake(withContext context: AnyObject?) {
        super.awake(withContext: context)
        
        if let log = mgr.motionLog{
            mgr.selectedReps = Int((log.mOriginalReps))
            mgr.selectedWeight = Int((log.mWeight))
        }
        
        var pickerItems: [WKPickerItem]! = []
        
        for index in 0...999{
            let item = WKPickerItem()
            item.title = index.description
            pickerItems.append(item)
        }
        
        repsPicker.setItems(pickerItems)
        weightPicker.setItems(pickerItems)
        
        // show chart
        if mgr.heartValue.count > 0{
            
            let frame = CGRect(x: 0.0, y: 0.0, width: self.contentFrame.size.width, height: self.contentFrame.size.width/2)
            let chart = NKBarChart.init(frame: frame)
            
            //chart?.barBackgroundColor = UIColor.black()
            chart?.labelMarginTop = 5.0
            chart?.showChartBorder = true
            chart?.xLabels = [Int](repeating: 1, count: mgr.heartValue.count)
            chart?.xLabelSkip = mgr.heartValue.count/3
            chart?.yValues = mgr.heartValue
            
            chart?.yMaxValue = CGFloat(mgr.heartValue.max()! + 5)
            chart?.yMinValue = CGFloat(mgr.heartValue.min()! - 5)
            chart?.strokeColor = UIColor.red()
            
            heartRateChart.setImage(chart?.drawImage())
        }
        
        
        guard let ctxObj = context as? [String : DetectionController] else {return}
        if let controller = ctxObj["controller"]{
            detectionController = controller
            detectionController?.summaryController = self
        }
    }
    

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if let log = mgr.motionLog{
            if let name = log.mOriginalBestMatchedExercise{
                nameLabel.setText(name)
            }else{
                nameLabel.setText(SummaryController.kNoNamePlaceholder)
                titleLabel.setText(SummaryController.kTitleCreateName)
            }
            consistencyLabel.setText(String(log.mConsistency) + "%")
            velocityLabel.setText(String(log.mVelocity) + "s")
            repsPicker.setSelectedItemIndex(Int(log.mOriginalReps))
            weightPicker.setSelectedItemIndex(Int(log.mWeight))
        }
        

        
        
        
//        NKBarChart *chart = [[NKBarChart alloc] initWithFrame:frame];
//        chart.yLabelFormatter = ^(CGFloat yValue){
//            CGFloat yValueParsed = yValue;
//            NSString * labelText = [NSString stringWithFormat:@"%0.f",yValueParsed];
//            return labelText;
//        };
//        chart.barBackgroundColor = [UIColor blackColor];
//        chart.labelMarginTop = 5.0;
//        chart.showChartBorder = YES;
//        [chart setXLabels:@[@"2",@"3",@"4",@"5",@"2",@"3",@"4",@"5",@"5",@"5",@"5",@"5",@"5",@"5",@"5",@"5",@"5",@"5",@"5",@"5"]];
//        chart.xLabelSkip = 6;
//        //       self.barChart.yLabels = @[@-10,@0,@10];
//        [chart setYValues:@[@10.82,@1.88,@6.96,@33.93,@10.82,@1.88,@6.96,@33.93,@10.82,@10.82,@10.82,@10.82,@10.82,@10.82,@10.82,@10.82,@10.82,@10.82,@10.82,@10.82]];
//        [chart setStrokeColors:@[NKGreen,NKGreen,NKRed,NKGreen,NKGreen,NKGreen,NKRed,NKGreen,NKGreen,NKGreen,NKGreen,NKGreen,NKGreen,NKGreen,NKGreen,NKGreen,NKGreen,NKGreen,NKGreen,NKGreen]];
//        
//        chart.yMinValue = 5;
//        
//        image = [chart drawImage];
    }
    
    override func didAppear() {
        super.didAppear()

    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    @IBAction func didPickReps(_ value: Int) {
        mgr.selectedReps = value
    }
    
    @IBAction func didPickWeight(_ value: Int) {
        mgr.selectedWeight = value
    }
    
    
    @IBAction func nameEntryTapped() {
        if let motionDetect = MotionDetect.sharedInstance() as? MotionDetect{
            var exerciseNames = [String]()
            
            if let orderedExer = motionDetect.getOrderedList(){
                for exer in orderedExer{
                    exerciseNames.append(exer["name"] as! String)
                }
            }
            self.presentTextInputController(withSuggestions: exerciseNames, allowedInputMode:
            .plain) { (results) -> Void in
                guard let results = results else {return} // there are chance of nil or no data
                if let selectedExercise = results.first as? String{
                    self.mgr.motionLog?.mBestMatchedExercise = selectedExercise
                    self.nameLabel.setText(selectedExercise)
                    self.titleLabel.setText(SummaryController.kTitleLogged)
                }
            }
        }
    }
    
    @IBAction func repsMinus() {
        repsPicker.focus()
        repsPicker.setSelectedItemIndex(mgr.selectedReps - 1)
    }
    
    @IBAction func repsPlus() {
        repsPicker.focus()
        repsPicker.setSelectedItemIndex(mgr.selectedReps + 1)
    }
    
    @IBAction func weightMinus() {
        weightPicker.focus()
        weightPicker.setSelectedItemIndex(mgr.selectedWeight - 1)
    }

    @IBAction func weightPlus() {
        weightPicker.focus()
        weightPicker.setSelectedItemIndex(mgr.selectedWeight + 1)
    }
    
    override func dismiss() {
        autoDismiss = true
        super.dismiss()
    }
    
    @IBAction func deleteEntry() {
        super.dismiss()
        mgr.motionLog = nil
    }
    
    deinit {
        
        //auto dismiss only happens when new workout is detected so switch to dectected state
        if autoDismiss{
            detectionController?.pendingScreenState = ScreenState.Detected
        }else{
            detectionController?.pendingScreenState = ScreenState.Detecting
        }
        detectionController?.switchScreenState()
    }
    
    
}

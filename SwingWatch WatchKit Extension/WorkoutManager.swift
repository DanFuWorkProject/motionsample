/*
 Copyright (C) 2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 This class manages the HealthKit interactions and provides a delegate 
         to indicate changes in data.
 */

import Foundation
import WatchKit
import HealthKit

/**
 `WorkoutManagerDelegate` exists to inform delegates of swing data changes.
 These updates can be used to populate the user interface.
 */
protocol WorkoutManagerDelegate: class {
//    func didUpdateForehandSwingCount(_ manager: WorkoutManager, forehandCount: Int)
//    func didUpdateBackhandSwingCount(_ manager: WorkoutManager, backhandCount: Int)
//    func didUpdateExerciseName(_ manager: WorkoutManager, exerciseName: String)
    func didUpdateHeartRate(_ manager: WorkoutManager, heartRate: String)
}

class WorkoutManager: NSObject, HKWorkoutSessionDelegate {
    
    // MARK: Properties
    let motionManager = MotionManager()
    var motionLog : LogState?
    var selectedReps = 0
    var selectedWeight = 0
    //var selectedUnit = WeightUnit.lbs
    
    let healthStore = HKHealthStore()
    let heartRateUnit = HKUnit(from: "count/min")
    var anchor = HKQueryAnchor(fromValue: Int(HKAnchoredObjectQueryNoAnchor))
    var heartRate = 0;
    
    //for logging
    var heartDate = [Date]()
    var heartValue = [Int]()

    weak var delegate: WorkoutManagerDelegate?
    var session: HKWorkoutSession?
    
    static let sharedInstance: WorkoutManager = WorkoutManager()

    // MARK: Initialization
    
    override init() {
        super.init()

        
        // Only proceed if health data is available.
        guard HKHealthStore.isHealthDataAvailable() else { return }
        
        // We need to be able to write workouts, so they display as a standalone workout in the Activity app on iPhone.
        // We also need to be able to write Active Energy Burned to write samples to HealthKit to later associating with our app.
        let typesToShare = Set([
            HKObjectType.workoutType(),
            HKObjectType.quantityType(forIdentifier: .activeEnergyBurned)!])
        
        let typesToRead = Set([
            HKObjectType.quantityType(forIdentifier: .activeEnergyBurned)!,
            HKObjectType.quantityType(forIdentifier: .heartRate)!])
        
        healthStore.requestAuthorization(toShare: typesToShare, read: typesToRead) { success, error in
            if let error = error where !success {
                print("You didn't allow HealthKit to access these read/write data types. In your app, try to handle this error gracefully when a user decides not to provide access. The error was: \(error.localizedDescription). If you're using a simulator, try it on a device.")
            }
        }
    }

    // MARK: WorkoutManager
    
    func startWorkout() {
        // If we have already started the workout, then do nothing.
        if (session != nil) {
            return
        }

        // Configure the workout session.
        let workoutConfiguration = HKWorkoutConfiguration()
        workoutConfiguration.activityType = .crossTraining
        workoutConfiguration.locationType = .indoor

        do {
            session = try HKWorkoutSession(configuration: workoutConfiguration)
            session?.delegate = self
        } catch {
            fatalError("Unable to create the workout session!")
        }

        // Start the workout session and device motion updates.
        healthStore.start(session!)
        motionManager.startUpdates()
    }

    func stopWorkout() {
        // If we have already stopped the workout, then do nothing.
        if (session == nil) {
            return
        }

        // Stop the device motion updates and workout session.
        motionManager.stopUpdates()
        healthStore.end(session!)

        // Clear the workout session.
        session = nil
        
        // Return to main screen when finish workout.
        WKInterfaceController.reloadRootControllers(withNames: ["main"], contexts:nil)
    }
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didChangeTo toState: HKWorkoutSessionState, from fromState: HKWorkoutSessionState, date: Date) {
        switch toState {
        case .running:
            workoutDidStart(date: date)
        case .ended:
            workoutDidEnd(date: date)
        default:
            //only one workout at once so maybe other workout might be started
            //simulator can start multiple workout
            print("Unexpected state \(toState)")
        }
    }
    
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: NSError) {
        // Do nothing for now
    }
    
    func workoutDidStart(date : NSDate) {
        if let query = createHeartRateStreamingQuery(workoutStartDate: date) {
            healthStore.execute(query)
        }
    }
    
    func workoutDidEnd(date : NSDate) {
        if let query = createHeartRateStreamingQuery(workoutStartDate: date) {
            healthStore.stop(query)
        }
    }
    
    func createHeartRateStreamingQuery(workoutStartDate: NSDate) -> HKQuery? {
        // adding predicate will not work
        // let predicate = HKQuery.predicateForSamplesWithStartDate(workoutStartDate, endDate: nil, options: HKQueryOptions.None)
        
        guard let quantityType = HKObjectType.quantityType(forIdentifier: .heartRate) else { return nil }
        
        let heartRateQuery = HKAnchoredObjectQuery(type: quantityType, predicate: nil, anchor: anchor, limit: Int(HKObjectQueryNoLimit)) { (query, sampleObjects, deletedObjects, newAnchor, error) -> Void in
            
            guard let newAnchor = newAnchor else {return}
            self.anchor = newAnchor
            self.updateHeartRate(samples: sampleObjects)
        }
        
        heartRateQuery.updateHandler = {(query, samples, deleteObjects, newAnchor, error) -> Void in
            self.anchor = newAnchor!
            self.updateHeartRate(samples: samples)
        }
        return heartRateQuery
    }
    
    func updateHeartRate(samples: [HKSample]?) {
        guard let heartRateSamples = samples as? [HKQuantitySample] else {return}
        
        DispatchQueue.main.async {
            guard let sample = heartRateSamples.first else{return}
            let value = sample.quantity.doubleValue(for: self.heartRateUnit)
            
            self.heartDate.append(Date())
            let logdate = Date().timeIntervalSince1970
            print("Date : \(logdate)")
            self.heartValue.append(Int(UInt16(value)))
            
            self.delegate?.didUpdateHeartRate(self, heartRate: (String(UInt16(value))))
//            if let detectionController = WKExtension.shared().rootInterfaceController as? DetectionController{
//                
//                self.heartRate = Int(value)
//                detectionController.heartRate = (String(UInt16(value)))
//            }
        }
    }
    
    


}

//
//  DetectionController.swift
//  vimofit
//
//  Created by Ethan Fan on 7/15/15.
//  Copyright © 2015 Vimo Lab. All rights reserved.
//

import Foundation
import WatchKit
import WatchConnectivity

//Screen State
enum ScreenState {
    case Detecting
    case Detected
    case Logged
}

class DetectionController : WKInterfaceController, WorkoutManagerDelegate, MotionDetectDelegate{
    
    var active = false
    var currentScreenState : ScreenState = .Detecting
    var pendingScreenState : ScreenState = .Detecting
    
    // detecing group
    @IBOutlet var detectingGroup: WKInterfaceGroup!
    
    // deteced group
    @IBOutlet var detectedGroup: WKInterfaceGroup!
    @IBOutlet var exerciseLabel: WKInterfaceLabel!
    @IBOutlet var repCountLabel: WKInterfaceLabel!
    @IBOutlet var logButton: WKInterfaceButton!
    
    //lazy var synthesizer = AVSpeechSynthesizer()
    
    weak var summaryController : SummaryController?
    
    func logSensorBuffer() {
        motionDetect.logSensorBuffer()
    }
    

    @IBOutlet var bpmLabel: WKInterfaceLabel!
    
    let extensionDelegate = WKExtension.shared().delegate as! ExtensionDelegate
    let motionDetect      = MotionDetect.sharedInstance() as! MotionDetect
    let mgr               = WorkoutManager.sharedInstance
    
    var templog : LogState?
    
    var reps = 0
    
    static let NEWNAME = "New motion"
    var name = DetectionController.NEWNAME
    var heartRate = "--"
    
    // MARK: Initialization
    
    override init() {
        super.init()
        
        mgr.startWorkout()
        motionDetect.delegate = self
        mgr.delegate = self
        
        // Schedule a timer to update the counter every second.
        let updateTimer = Timer(timeInterval: 1, target: self, selector: #selector(DetectionController.updateUI), userInfo: nil, repeats: true)
        RunLoop.current.add(updateTimer, forMode: .commonModes)
    }
    
    override func awake(withContext context: AnyObject?) {
        super.awake(withContext: context)
        becomeCurrentPage()
        addMenuItem(with: .trash, title: "Delete Motion", action: #selector(self.showMotionsTable) )

    }
    
    override func willActivate() {
        super.willActivate()
        //addMenuItem(with: .trash, title: "Delete Motion", action: #selector(self.showMotionsTable) )
        active = true
        updateUI()
    }
    
    override func didAppear() {
        super.didAppear()
        print(#function)
    }
    
    override func willDisappear() {
        super.willDisappear()
    }
    
    func updateUI(){
        
        switchScreenState()
        updateLabels()
    }
    
    
    func switchScreenState(){
        
        // Don't switch state if the screen is not active and pendingScreen same as current
        if pendingScreenState == currentScreenState || active == false{
            return
        }
        
        currentScreenState = pendingScreenState
    
        switch currentScreenState {
        case .Detecting:
            //animate(withDuration: 1, animations: {
                self.reps = 0
                self.name = DetectionController.NEWNAME
                self.detectingGroup.setHidden(false)
                self.detectedGroup.setHidden(true)
            //})
            clearAllMenuItems()
            addMenuItem(with: .trash, title: "Delete Motion", action: #selector(self.showMotionsTable) )
        case .Detected:
            //animate(withDuration: 1, animations: {
                self.detectingGroup.setHidden(true)
                self.detectedGroup.setHidden(false)
            //})
            clearAllMenuItems()
            addMenuItem(with: .decline, title: "Reset", action: #selector(self.resetReps) )
            
        case .Logged:
            WKInterfaceDevice.current().play(.success)
            self.presentController(withName: "summary", context: ["controller": self])
        }
    }
    
    func resetReps(){
        motionDetect.reset()
        setDetecting()
    }
    
    func showMotionsTable(){
        presentController(withName: "edit", context: nil)
    }

    override func didDeactivate() {
        super.didDeactivate()
        active = false
    }
    
    @IBAction func logClicked() {

        self.mgr.motionLog = self.motionDetect.getLogDetails()
        pendingScreenState = .Logged
        switchScreenState()
        
    }
        
    // for debbuging send to Parse
    @IBAction func menuLog() {
        //WCSession.defaultSession().transferUserInfo(["debug" : VMEngine.sharedInstance.logs])
    }
    
    // MARK: Convenience
    
    func updateLabels() {
        if active{
            exerciseLabel.setText(name)
            repCountLabel.setText(String(format: "%02d", reps))
            bpmLabel.setText(heartRate)
        }
    }
    
    // =========================================================================
    
    // MARK: - WorkoutManagerDelgate
    
    func didUpdateHeartRate(_ manager: WorkoutManager, heartRate: String) {
        self.heartRate = heartRate
    }
    
    // MARK: - MotionDetectDelgate
    
    // will get call back when new motion detected but not logged for a while
    func setDetecting() {
        print(#function)
        DispatchQueue.main.async {
            // don't switch screen if in logged screccn
            if self.currentScreenState == .Logged{
                return
            }else{
                self.pendingScreenState = .Detecting
                self.switchScreenState()
            }
        }
    }
    
    func reportRep(_ reps: Int32, wDetector detector: Int32) {
        print(#function)
        DispatchQueue.main.async {
            if self.pendingScreenState != .Detected{
                self.pendingScreenState = .Detected
                self.switchScreenState()
            }
            self.reps = Int(reps)
            self.updateLabels()
            
            // todo: check setting for play hatic
//            if reps % 5 == 0{
//                let utterance = AVSpeechUtterance(string: "\(self.reps)")
//                utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
//                utterance.rate = 0.5
//                self.synthesizer.speak(utterance)
//            }
            
        }
    }
    
    func reportDetectedExercise(_ name: String!) {
        print(#function)
        DispatchQueue.main.async {
            self.name = name
            self.updateLabels()
        }
    }
    
    // Tell user we have recorded an exercise. This function should vibrate, then bring up edit interface
    func showExerciseLogged() {
        print(#function)
        DispatchQueue.main.async {
            // don't show summary again if screen is already in logged state
            if self.currentScreenState == .Logged{
                return
            }else{
                self.mgr.motionLog = self.motionDetect.getLogDetails()
                self.pendingScreenState = .Logged
                self.switchScreenState()
            }
        }
    }
    
    // Save log to buffer
    func logExercise() {
        print(#function)
        DispatchQueue.main.async {
            
            //clear heart rate
            self.mgr.heartValue.removeAll()
            self.mgr.heartDate.removeAll()
            
            if self.currentScreenState == .Logged{
                self.summaryController?.dismiss()
            }
            
            //save the final state here
            if let log = self.mgr.motionLog{
                // read unit from setting
                if log.mBestMatchedExercise != nil{ //avoid crash when no name so don't save if no new name given
                    self.motionDetect.saved(log, atReps: Int32(self.mgr.selectedReps), atWeight: Int32(self.mgr.selectedWeight), atUnit: 0)
                }
            }
            
            //clear out all cache
            self.mgr.selectedReps = 0
            self.mgr.selectedWeight = 0
            self.reps = 0
            self.name = DetectionController.NEWNAME
            self.mgr.motionLog = nil
        }
        
    }
    
    var logs = ""
    func addLog(_ log: String!) {
        logs += " " + log
    }

}

//
//  MotionTableController.swift
//  Tracker
//
//  Created by Ethan Fan on 7/5/16.
//  Copyright © 2016 Apple Inc. All rights reserved.
//

import WatchKit
import Foundation


class MotionTableController: WKInterfaceController {

    @IBOutlet var table: WKInterfaceTable!
    
    let motionDetect      = MotionDetect.sharedInstance() as! MotionDetect
    let mgr               = WorkoutManager.sharedInstance
    var tableLoaded       = false
    var names = [String]()
    
    override func awake(withContext context: AnyObject?) {
        super.awake(withContext: context)
        
    
        // Configure interface objects here.
        
        // Todo: stop sensor
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if !tableLoaded{
            loadTableData()
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    private func loadTableData() {
        let count = motionDetect.motionLibrary.count
        if let _ = motionDetect.motionLibrary {
            for key in motionDetect.motionLibrary.allKeys {
                names.append(key as! String)
            }
        }
        
        table.setNumberOfRows(count, withRowType: "Cell")
        
        for i in 0 ..< count {
            if let row = table.rowController(at: i) as? MotionRowController{
                row.showItem(title: names[i])
            }
        }
        
        tableLoaded = true
        
    }
    
    // MARK: WKInterfaceTable
    // Select row to be remove
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        
        let cancel = WKAlertAction(title: "Cancel", style: .cancel, handler: {
        
        })

        
        let action = WKAlertAction(title: "Delete", style: .destructive, handler: { () -> Void in
            table.removeRows(at: NSIndexSet(index: rowIndex) as IndexSet)
            
            self.motionDetect.motionLibrary.removeObject(forKey: self.names[rowIndex])
            // persist
            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: self.motionDetect.motionLibrary), forKey: "library")
            UserDefaults.standard.synchronize()
        })
        
        presentAlert(withTitle: "Delete Motion", message: "Are you sure you want to delete \(self.names[rowIndex])?", preferredStyle: .sideBySideButtonsAlert, actions: [cancel,action])
        
    }


}

//
//  MotionDetect.m
//  vimofit
//
//  Created by Huan Liu on 7/4/15.
//  Copyright © 2015 Vimo Lab. All rights reserved.
//

#import "MotionDetect.h"
#import "PeakValleyDetector.h"
#import <CoreMotion/CoreMotion.h>
#import "Motion.h"

#import <Parse/Parse.h>


// internally used class
@interface DetectState : NSObject

@property int mLastMatchedPV;  // record
@property int mReps;
@property int mInt;   // number of interruptions
@property float mRange;    // The magnitude of movement, the bigger range, the better the signal. Used to select detector.
@property double mAvgDetectorScore, mBestScore, mAvgScore;

@property Boolean mInterrupted;  // no match found initially
@property Boolean mWasTentativelyIncremented;   // was the last rep added tentative?

@property int mInterruptPosition;
@property int mStartPosition;
@property long mStartTime;   // for cardio activity, it is good to record when started
@property int mLastRepPos;   // position where last rep ended
@property float ** mWaveform;
@property int mWaveformX, mWaveformY;  // dimensions of the mWaveform array

@property int mLastRepLength;

@end

@implementation DetectState
- (id) init {
    self = [super init];
    if (self) {
        self.mReps = 0;
        self.mInterrupted = true;
        self.mInterruptPosition = -1;
    }
    return self;
}

- (void)dealloc {
    [MotionDetect deallocFloatArray:self.mWaveform ];
    self.mWaveform = nil;
}
@end

@implementation LogState
- (id) init {
    self = [super init];
    if (self) {
        self.mWaveform = nil;
    }
    return self;
}
// make sure no memory leak
- (void)dealloc {
    if (self.mWaveform != nil) {
        [MotionDetect deallocFloatArray:self.mWaveform ];
        self.mWaveform = nil;
    }
}
@end

const int BUFFER_SIZE=1200;

// sampling rate, and the inverse, interval between sample
const int SAMPLE_INTERVAL = 50000;    // in us
const int SAMPLE_RATE = 20;    // per second

// Circular buffer to store past sensor data
const int NUM_DETECTORS = 7;

const int SAVE_DIMENSION = 20;

const int REP_REPORT_THRESHOLD = 3; // there is noise, easy to get two consecutive similar moves. This is a threshold where must get X consecutive move before reporting
const int REP_CONT_THRESHOLD = 10;  // when do we allow rep to break in the middle

// push up with rotation takes long
const int INACTIVITY_THRESHOLD = SAMPLE_RATE * 15; // how soon must perform a rep, used to avoid long false cycle
const int BREAK_THRESHOLD = SAMPLE_RATE * 5; // how long we allow user to take a quick break without breaking rep sequence, this is regardless of how many reps they have done
const int REP_TIME_DIFF = SAMPLE_RATE * 5; // how much we allow time warping, to save computation time

const int REP_COMP_DIFF_FACTOR = 3; // how length-wise different two reps can be before we refuse to compare
const int EXERCISE_TIMEOUT_FACTOR = 3; // how long we wait before declare user done with this exercise, expressed in rep multiples
const float MATCH_FUZZ_FACTOR = 1.2f;

// threshold for ratio, which takes into account the range of motion
const float MATCH_SCORE_THRESHOLD = 0.04f;
const float DETECTOR_SCORE_THRESHOLD = 0.04f;
const float GOOD_SCORE_THRESHOLD = 0.06f;
// threshold for raw comparison score
const float MATCH_SCORE_RAW_THRESHOLD = 1.0f;
const float DETECTOR_SCORE_RAW_THRESHOLD = 1.0f;

const float EARTH_GRAVITY = 9.85f;  // should convert to m/s^2 for any accel fields, so that threshold is consistent with Android
const BOOL debug = false;

@interface MotionDetect () {
//    float[] mRotationMatrix = new float[9];
    
    float **mCircularBuffer;  // 3 gravity, 3 gyro, ZAC, LIN0, LIN1, Roto_vector
    long mBufferTimestamp[BUFFER_SIZE];
    int mPos;
    
    const float * mDetectorToleranceFactor;
    const float * mMotionToleranceFactor;
    const float * mRangeReductionFactor;
    
    // rep detection algorithms, run them all since we do not know which one works if we do not know the exercise
    PeakValleyDetector * mDetectors[NUM_DETECTORS + 3];
    PeakValleyDetector * mBacktrackDetector;
    
//    public class MatchScore {
//        float score;
//        float range;
//    }
    
    DetectState * mDetectState[NUM_DETECTORS];
    DetectState * mSaveState;             // the detector state that needs to be saved. Need a separate variable because mDetectState may have moved on to next move
    int mPV[NUM_DETECTORS];
    int mCurrentDetector;
    int mSaveDetector;   // the detector to use when saving motion. It should be synced with what is displayed to user, i.e., whenever update UI, update this
    
    NSString * mDetectedExerciseName;
    
    int _currentMatchNum;
    NSArray * mDetectedExercises;
    
    long mBaseTimestamp;
}
@end


@implementation MotionDetect

static const float constList[] = {1, 1, 1, 1, 1, 1, 1.5, 1.5};
static const float constList1[] = {1, 1, 1, 1, 1, 1, 1, 1};
static const float constList2[] = {1, 1, 1, 1, 1, 1, 2.5, 2.5};

- (id) init {
    self = [super init];
    if (self) {
        mDetectorToleranceFactor = constList;
        mMotionToleranceFactor = constList1;
        mRangeReductionFactor = constList2;
        
        mCircularBuffer = [MotionDetect allocFloatArrayX:SAVE_DIMENSION Y:BUFFER_SIZE];
        
        // setup detectors
        // note currently the gap is not enforced
        // TODO, reconsider GRV1S GRV2S, only two exercises need them: ArmHauler & Clam, or LIN1S/ACC1S, only lunges use it
        mDetectors[0] = [[PeakValleyDetector alloc] initWithLowPassFilter:false];
        mDetectors[1] = [[PeakValleyDetector alloc] initWithLowPassFilter:false];
        mDetectors[2] = [[PeakValleyDetector alloc] initWithLowPassFilter:false];
        mDetectors[3] = [[PeakValleyDetector alloc] initWithLowPassFilter:false];
        // slot 4,5 are empty to match compare dimension with detect dimension
        mDetectors[6] = [[PeakValleyDetector alloc] initWithLowPassFilter:true];  // exercise like StepUp has a lot of noise, need to LPF to remove it
        mDetectors[7] = [[PeakValleyDetector alloc] initWithLowPassFilter:true];
        mDetectors[8] = [[PeakValleyDetector alloc] initWithLowPassFilter:true];
        mDetectors[9] = [[PeakValleyDetector alloc] initWithLowPassFilter:true];
        
        // 50% of this is the absolute minimum threshold should be used
        mDetectors[0].threshold = 12.5;//15;   // GYR0A only Punch front kick and Arnold Press use this
        mDetectors[1].threshold = 3;//15;  // GYR1A   // higher threshold in Y direction to avoid noise
        mDetectors[2].threshold = 2.5;//8;  // GYR2A    // more GYR2 movement, this is a good value for push up
        mDetectors[3].threshold = 7;//5; // GRV0S
        // repeat for other cycle constant
        mDetectors[6].threshold = 6;//8;  // ZAC
        mDetectors[7].threshold = 7;//8;  // LIN0S
        mDetectors[8].threshold = 10000; // do not match
        mDetectors[9].threshold = 10000;
        
        mBacktrackDetector = [[PeakValleyDetector alloc] initWithLowPassFilter:false];
        
        // This is a percentage of the min-max range that should be used as threshold, but must be greater than the minimum
        mDetectors[0].thresholdPercent = 0.55f;
        mDetectors[1].thresholdPercent = 0.55f;
        mDetectors[2].thresholdPercent = 0.55f;
        mDetectors[3].thresholdPercent = 0.55f;
        mDetectors[6].thresholdPercent = 0.4f;
        mDetectors[7].thresholdPercent = 0.4f;
        
        // a smaller MR window will remove influence from a bad big gravity sample faster
        // a bigger MR window will smooth out the jitter
        mDetectors[6].mMRInterval = (int) (1.5 * SAMPLE_RATE);  // ZAC0D
        mDetectors[7].mMRInterval = (int) (1.5 * SAMPLE_RATE);  // LIN0D
        mDetectors[8].mMRInterval = (int) (1.5 * SAMPLE_RATE);  // LIN1D
        mDetectors[9].mMRInterval = (int) (1.5 * SAMPLE_RATE);  // LIN2D
        
        
        // init
        for (int i = 0; i < NUM_DETECTORS; i++)
            mDetectState[i] = [[DetectState alloc] init];
        
        // load motion library
        NSData * data = [[NSUserDefaults standardUserDefaults] objectForKey:@"library"];
        if (!data) {  // if no user saved library, read from bundle the default motion library
            NSString *fileRoot = [[NSBundle mainBundle] pathForResource:@"motionlib" ofType:@"bin"];
            data = [NSData dataWithContentsOfFile:fileRoot];
        }
        self.motionLibrary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if (self.motionLibrary == nil)
            self.motionLibrary = [[NSMutableDictionary alloc] init];
        
//        [self test];
        
//        Motion * motion = [[Motion alloc] init];
//        motion.mName = @"huan";
//        motion.mDetector[2] = 100;
//        motion.wavePtr = 2;
//        motion.mBuffer[2] = [MotionDetect allocFloatArrayX:5 Y:10];
//        motion.mBufferX[2] = 5;
//        motion.mBufferY[2] = 10;
//        
//        motion.mBuffer[2][0][0] = 1;
//        motion.mBuffer[2][4][0] = 2;
//        motion.mBuffer[2][0][1] = 3;
//        motion.mBuffer[2][4][1] = 4;
//        motion.mBuffer[2][3][3] = 5;
//        motion.mBuffer[2][2][8] = 6;
//        
//        NSData * aa = [NSKeyedArchiver archivedDataWithRootObject:motion];
//        
//        Motion * newm = [NSKeyedUnarchiver unarchiveObjectWithData:aa];
//        
//        NSLog(@"buffer %f %f %f %f %f %f", newm.mBuffer[2][0][0], newm.mBuffer[2][4][0], newm.mBuffer[2][0][1], newm.mBuffer[2][4][1], newm.mBuffer[2][3][3], newm.mBuffer[2][2][8]);
//        [self saved:@"close grip bench" from:nil rep:12 from:5 weight:10];

    }
    
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        [self drive];
//    });
    
    return self;
}

+ (id)sharedInstance
{
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

- (void) dealloc {
    [MotionDetect deallocFloatArray:mCircularBuffer];
    mCircularBuffer = nil;
}

// Utility routines for two-dimension array allocation/deallocation
+ (float **) allocFloatArrayX:(int) sx Y:(int) sy {
    float * a1 = malloc(sx*sy*sizeof(float));
    float ** array = malloc(sx*sizeof(float*));
    for (int row=0; row<sx; row++)
        array[row]=a1+row*sy;
    return array;
}
+ (void) deallocFloatArray:(float **) array {
    if (array != nil) {
        float * a1 = array[0];
        free(array);
        free(a1);
    }
}
+ (float**) dupFloatArray:(float **) array X:(int) sx Y:(int) sy {
    if (array != nil) {
        float ** newarray = [MotionDetect allocFloatArrayX:sx Y:sy];
        for (int i=0 ; i<sx ; i++)
            for (int j=0 ; j<sy ; j++)
                newarray[i][j] = array[i][j];
        return newarray;
    }
    return nil;
}

- (LogState *) getLogDetails {
    if (mSaveState==nil)
        return nil;
    
    LogState * logstate = [[LogState alloc] init];
    
    long length = [NSDate timeIntervalSinceReferenceDate]*1000 - mSaveState.mStartTime;
    // exact time:   (new Date()).getTime() - mSaveState.mStartTime - (mBufferTimestamp[mPos % BUFFER_SIZE] - mBufferTimestamp[mSaveState.mLastRepPos % BUFFER_SIZE]));
    
    logstate.mExercises = [self getOrderedList];
    logstate.mWaveform = [MotionDetect dupFloatArray:mSaveState.mWaveform X:mSaveState.mWaveformX Y:mSaveState.mWaveformY] ;
    logstate.mWaveformX = mSaveState.mWaveformX;
    logstate.mWaveformY = mSaveState.mWaveformY;
    logstate.mStartTime = mSaveState.mStartTime;
    logstate.mLength = length;
    logstate.mBestMatchedExercise = mDetectedExerciseName;
    logstate.mOriginalBestMatchedExercise = mDetectedExerciseName;
    logstate.mOriginalReps = mSaveState.mReps;
    logstate.mDetector = mSaveDetector;
    
    logstate.mConsistency = (int) (((MATCH_SCORE_THRESHOLD - mSaveState.mAvgScore)/MATCH_SCORE_THRESHOLD)*50+50);
    if (logstate.mConsistency>100)
        logstate.mConsistency=100;
    
    logstate.mVelocity = (float)mSaveState.mLastRepLength/(float)SAMPLE_RATE;
    
    // look up last weight
    Motion * m = [self.motionLibrary objectForKey:mDetectedExerciseName];
    logstate.mWeight = 0;
    if ( m!=nil )
        logstate.mWeight = m.mLastWeight;
    
    return logstate;
}

// save log to Parse
- (void) saved:(LogState *) logState atReps:(int)savedReps atWeight:(int)savedWeight atUnit:(int) savedWeightUnit {
    // cannot log if there is no name
    if (logState.mBestMatchedExercise==nil)
        return;
    
    // if we detected wrong, user would have chosen a different name and rep, need to remember motion
    Motion * motion = [self.motionLibrary objectForKey:logState.mBestMatchedExercise];
    if (motion) {    // existing motion. update motion if needed
        if (logState && logState.mOriginalReps!=0) {  // skip motion update if manual logging.  Manual logging <===> mOriginalReps==0
            // mSaveDetector should never be -1, we have not detected an exercise, but somehow calling routine may do this, we skip

            // update motion if necessary
            if (![logState.mBestMatchedExercise isEqualToString:logState.mOriginalBestMatchedExercise]) {
                // now the closely matched motion is selected, update the waveform, as this presumably is what the user think he is doing
                motion.mBuffer[motion.wavePtr] = logState.mWaveform;
                motion.mBufferX[motion.wavePtr] = logState.mWaveformX;
                motion.mBufferY[motion.wavePtr] = logState.mWaveformY;
                logState.mWaveform = nil;    // prevent it from being deallocated
                motion.mDetector[motion.wavePtr] = logState.mDetector;
                motion.wavePtr = (motion.wavePtr + 1) % NUM_SAVED_MOTION;
            }
        }
    }
    else {  // does not exist, create new, and save waveform
        motion = [[Motion alloc] init];
        motion.mName = logState.mBestMatchedExercise;
        if (logState && logState.mOriginalReps!=0) { // do not update if manual logging, or if we have no motion to save
            // mSaveDetector should never be -1, we have not detected an exercise, but somehow calling routine may do this, we skip

            motion.mBuffer[motion.wavePtr] = logState.mWaveform;
            motion.mBufferX[motion.wavePtr] = logState.mWaveformX;
            motion.mBufferY[motion.wavePtr] = logState.mWaveformY;
            logState.mWaveform = nil;    // prevent it from being deallocated
            motion.mDetector[motion.wavePtr] = logState.mDetector;
            motion.wavePtr = (motion.wavePtr + 1) % NUM_SAVED_MOTION;
        }
        [self.motionLibrary setObject:motion forKey:logState.mBestMatchedExercise];
    }

    motion.mLastWeight = savedWeight;
    motion.mLastWeightUnit = savedWeightUnit;
    motion.mLastRecordTime = ([NSDate timeIntervalSinceReferenceDate] + NSTimeIntervalSince1970) * 1000;  // for no motion exercises, need to know recentcy to reorder them for convenience. In milli-seconds
    motion.mLastReps = savedReps;      // for no motion exercises, scroll to lastRep for convenience

    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:self.motionLibrary] forKey:@"library"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // save motion library in file
//    NSData * ddd = [NSKeyedArchiver archivedDataWithRootObject:self.motionLibrary];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *docDir = [paths objectAtIndex: 0];
//    NSString* docFile = [docDir stringByAppendingPathComponent: @"motionlib"];
//    [ddd writeToFile:docFile atomically:false];
//    
//    PFFile * file = [PFFile fileWithName:@"motionlib" data:ddd];
//    [file saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//        if (error == nil) {
//            PFObject * holder = [PFObject objectWithClassName:@"Motion"];
//            [holder setObject:file forKey:@"file"];
//            [holder saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//                NSLog(@"success %i %@", succeeded, error);
//            }];
//        }
//    }];
}

- (void) logSensorBuffer {
    
    NSMutableString * str = [[NSMutableString alloc] init];
    for (int i=0 ; i<BUFFER_SIZE ; i++) {
        for (int j=0 ; j<SAVE_DIMENSION ; j++) {
            [str appendFormat:@"%f ", mCircularBuffer[j][i]];
        }
        [str appendFormat:@"%ld \n", mBufferTimestamp[i]];
    }
        
    NSData * ddd = [str dataUsingEncoding:kCFStringEncodingUTF8];
    PFFile * file = [PFFile fileWithName:@"sensorBuffer" data:ddd];
    [file saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (error == nil) {
            PFObject * holder = [PFObject objectWithClassName:@"Motion"];
            [holder setObject:file forKey:@"file"];
            [holder saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                NSLog(@"success %i %@", succeeded, error);
            }];
        }
    }];

}

- (NSArray<NSDictionary<NSString *, id> *> *) getOrderedList {
    // TODO, put motionless exercises on bottom??
    
    NSMutableArray * namelist = [[NSMutableArray alloc] init];
    for (NSDictionary * dic in mDetectedExercises) {
        NSString * name = [dic objectForKey:@"name"];
        Motion * motion = [self.motionLibrary objectForKey:name];
        [namelist addObject:@{@"name":name, @"weight":@(motion.mLastWeight), @"weightunit":@(motion.mLastWeightUnit)}];
    }
    return namelist;
}

- (NSArray<NSDictionary<NSString *, id> *> *) getListOrderedByRecency {
    // TODO, put motionless exercises on top, because this is for manual input
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"mLastRecordTime" ascending:NO];
    NSArray * sortedArray=[[self.motionLibrary allValues] sortedArrayUsingDescriptors:@[sort]];
    
    NSMutableArray * namelist = [[NSMutableArray alloc] init];
    for (Motion * motion in sortedArray)
        [namelist addObject:@{@"name":motion.mName, @"weight":@(motion.mLastWeight), @"weightunit":@(motion.mLastWeightUnit), @"reps":@(motion.mLastReps)}];
    return namelist;
}

- (void) reset {
    mPos = 0;
    for (int i = 0; i < NUM_DETECTORS; i++) {
        if (!mDetectors[i]) continue;
        
        // TODO have to clear the moving average buffer etc.
        
        // reset detector state
        mDetectState[i].mWasTentativelyIncremented = false;
        mDetectState[i].mInterrupted = true;
        mDetectState[i].mInterruptPosition = -1;
        mDetectState[i].mReps = 0;
        
        mDetectState[i].mAvgDetectorScore = 0;
        mDetectState[i].mAvgScore = 0;
        mDetectState[i].mInt = 0;
        mDetectState[i].mLastRepPos = 0;
        
        [MotionDetect deallocFloatArray:mDetectState[i].mWaveform];   // free up memory if allocated before
        mDetectState[i].mWaveform = nil;
    }
    for (int i = 0; i < NUM_DETECTORS + 3; i++) {
        if (!mDetectors[i]) continue;
        // reset detector
        [mDetectors[i] reset];
    }
    mCurrentDetector = -1;
    mSaveDetector = -1;
    mSaveState = nil;
    
    // Cancel any pending matching threads
    _currentMatchNum ++ ;  // older match threads will see this and terminate
}

- (void) test {
    NSString* filePath = @"closegripbench-6o8-7d";
    NSString* fileRoot = [[NSBundle mainBundle] pathForResource:filePath ofType:@"txt"];
    
    NSString* fileContents = [NSString stringWithContentsOfFile:fileRoot encoding:NSUTF8StringEncoding error:nil];
    
    // first, separate by new line
    NSArray* allLinedStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    // then break down even further
    for (int i=0 ; i<=700 ; i++) {
        NSString * strsInOneLine = [allLinedStrings objectAtIndex:i];
    
    // choose whatever input identity you have decided. in this case ;
        NSArray* singleStrs = [strsInOneLine componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
        mCircularBuffer[0][i] = [[singleStrs objectAtIndex:0] floatValue];
        mCircularBuffer[1][i] = [[singleStrs objectAtIndex:1] floatValue];
        mCircularBuffer[2][i] = [[singleStrs objectAtIndex:2] floatValue];
//        mCircularBuffer[3][i] = [[singleStrs objectAtIndex:3] floatValue];
//        mCircularBuffer[4][i] = [[singleStrs objectAtIndex:4] floatValue];
//        mCircularBuffer[5][i] = [[singleStrs objectAtIndex:5] floatValue];
//        mCircularBuffer[6][i] = [[singleStrs objectAtIndex:6] floatValue];
//        mCircularBuffer[7][i] = [[singleStrs objectAtIndex:7] floatValue];
    }
}

- (void) drive {

    for (int i=0 ; i<NUM_DETECTORS ; i++)
        if (mDetectors[i]!=nil)
            mDetectors[i].lastindex = 200-1;
    
    for (int i=200 ; i<=700 ; i++) {
        mPos = i % BUFFER_SIZE;
        [self postNewSensor:nil];
    }

}





- (void) postNewSensor:(CMDeviceMotion *) deviceMotion {
    float mAX = (deviceMotion.userAcceleration.x + deviceMotion.gravity.x) * EARTH_GRAVITY;
    float mAY = (deviceMotion.userAcceleration.y + deviceMotion.gravity.y) * EARTH_GRAVITY;
    float mAZ = (deviceMotion.userAcceleration.z + deviceMotion.gravity.z) * EARTH_GRAVITY;
    
    float mGX = deviceMotion.gravity.x * EARTH_GRAVITY;
    float mGY = deviceMotion.gravity.y * EARTH_GRAVITY;
    float mGZ = deviceMotion.gravity.z * EARTH_GRAVITY;
    
    [self detectMotionrx:deviceMotion.rotationRate.x ry:deviceMotion.rotationRate.y rz:deviceMotion.rotationRate.z
                      ax:mAX ay:mAY az:mAZ gx:mGX gy:mGY gz:mGZ time:deviceMotion.timestamp*1000];
}


- (void) detectMotionrx:(float) mRX  ry:(float) mRY rz:(float) mRZ
                     ax:(float) mAX  ay:(float) mAY az:(float) mAZ
                     gx:(float) mGX  gy:(float) mGY gz:(float) mGZ
                   time:(long) timestamp {
    
    // advance pointer first. This makes it 1-based, matching the counting in PeakValleyDetector
    // use the gyro's sampling frequency, otherwise, would not match the detectors
    if (!debug) {
        if (mPos == 0) {
            // DOUBLE CHECK timestamp is in milliseconds
            mBaseTimestamp = timestamp;
        }
        
        // On Sony watch, Gyro over samples at 100Hz, no matter what we set
        int pos = (int) ((timestamp - mBaseTimestamp) * 1000.0f / SAMPLE_INTERVAL) + 1;
        if (pos >= mPos) { // could be caused by rounding, force to next spot
            //                        if (pos > mPos+2)  // arrive too infrequent, we missed a sample
            //                            logProgress("UnderSample " + mPos + " " + pos + " " + timestamp + " " + mBaseTimestamp);
            
            mPos = mPos + 1;
        } else if (pos < mPos) { // got too many samples in between our desired interval, do not log it yet, < because last time we over logged one
            //                        logProgress("DiscardOverSample " + mPos + " " + pos + " " + timestamp + " " + mBaseTimestamp);
            return;
        }
        
        mBufferTimestamp[mPos % BUFFER_SIZE] = timestamp;
    }

    
    mPV[0] = [mDetectors[0] postNewDSample:mRX at:timestamp]; // GYR0A
    mPV[1] = [mDetectors[1] postNewDSample:mRY at:timestamp]; // GYR1A
    mPV[2] = [mDetectors[2] postNewDSample:mRZ at:timestamp]; // GYR2A
    // record gyro degree
    if (!debug) {   // during debug, we compute diff, if we override, diff is wrong
        mCircularBuffer[0][mPos % BUFFER_SIZE] = (float) mDetectors[0].lastsample;
        mCircularBuffer[1][mPos % BUFFER_SIZE] = (float) mDetectors[1].lastsample;
        mCircularBuffer[2][mPos % BUFFER_SIZE] = (float) mDetectors[2].lastsample;
    }
    
    // use derived gravity vector
    mCircularBuffer[3][mPos % BUFFER_SIZE] = (float) mGX;
    mCircularBuffer[4][mPos % BUFFER_SIZE] = (float) mGY;
    mCircularBuffer[5][mPos % BUFFER_SIZE] = (float) mGZ;
    
    mPV[3] = [mDetectors[3] postNewSample:mCircularBuffer[3][mPos % BUFFER_SIZE] at:timestamp]; // GRV0S
    
    
    // ZAC
    float zaclen = (float) pow(mGX * mGX + mGY * mGY + mGZ * mGZ, 0.5);
    float zac = 0;
    if (zaclen > 0)
        zac = (float) (mAX * mGX / zaclen + mAY * mGY / zaclen + mAZ * mGZ / zaclen);
    mPV[6] = [mDetectors[6] postNewDDSample:zac at:timestamp]; // ZAC0D
    mCircularBuffer[6][mPos % BUFFER_SIZE] = (float) mDetectors[6].lastsample;
    
    // LIN0D
    [mDetectors[7] postNewDDSample:mAX - mCircularBuffer[3][mPos % BUFFER_SIZE] at:timestamp];
    mCircularBuffer[7][mPos % BUFFER_SIZE] = (float) mDetectors[7].lastsample;
    
    // LIN1D
    [mDetectors[8] postNewDDSample:mAY - mCircularBuffer[4][mPos % BUFFER_SIZE] at:timestamp];
    mCircularBuffer[8][mPos % BUFFER_SIZE] = (float) mDetectors[8].lastsample;
    // LIN2D
    [mDetectors[9] postNewDDSample:mAZ - mCircularBuffer[5][mPos % BUFFER_SIZE] at:timestamp];
    mCircularBuffer[9][mPos % BUFFER_SIZE] = (float) mDetectors[9].lastsample;
    
    
    //mCircularBuffer[13][mPos % BUFFER_SIZE] = event.values[3];
    mCircularBuffer[14][mPos % BUFFER_SIZE] = zac;   // ZAC
    if (!debug) {        // in debug mode, may be size 18, old format
        mCircularBuffer[15][mPos % BUFFER_SIZE] = mAX - mCircularBuffer[3][mPos % BUFFER_SIZE];  // LIN0D
        mCircularBuffer[16][mPos % BUFFER_SIZE] = mAY - mCircularBuffer[4][mPos % BUFFER_SIZE];  // LIN1D
        mCircularBuffer[17][mPos % BUFFER_SIZE] = mAX;
        mCircularBuffer[18][mPos % BUFFER_SIZE] = mAY;
        mCircularBuffer[19][mPos % BUFFER_SIZE] = mAZ;
    }
    
    // did we stop the exercise? only for identified exercise
    if (mSaveState!=nil &&
        mDetectors[0].lastindex - mSaveState.mLastRepPos == EXERCISE_TIMEOUT_FACTOR * mSaveState.mLastRepLength &&
        mDetectedExerciseName != nil) {
        
        [self.delegate showExerciseLogged];     // user stopped the exercise, vibrate to let user know, allow user to edit further
    }
    
    // clear everything if user does not log within a time limit
    if (mSaveDetector != -1 && mDetectors[mSaveDetector].lastindex - mDetectState[mSaveDetector].mLastRepPos > INACTIVITY_THRESHOLD &&
        mDetectedExerciseName == nil) {  // only clear when no exercise is detected
//        if (mMotionMatcher != null)
//            mMotionMatcher.cancel(true);   // cancel last match because detector is gone
        mSaveDetector = -1;
        mSaveState = nil;
        mCurrentDetector = -1;   // if currentDetector!=-1, saveDetector==currentDetector, so safe to clear both
        
        [self.delegate setDetecting];
    }
        
    for (int i = 0; i < NUM_DETECTORS; i++) {
        if (mDetectors[i] == nil) continue;
        
        float matchScore = 100;
        
        int p0 = -1, p1 = -1, p2 = -1, p3;
        // find the last two reps to compare
        if (mPV[i] == PEAK && mDetectors[i].mLastNPeakPos[1] != -1) {
            p2 = mDetectors[i].mLastNPeakPos[1];
            p1 = mDetectors[i].mLastNPeakPos[0];
            p0 = mDetectors[i].peakPos;
        } else if (mPV[i] == VALLEY && mDetectors[i].mLastNValleyPos[1] != -1) {
            p2 = mDetectors[i].mLastNValleyPos[1];
            p1 = mDetectors[i].mLastNValleyPos[0];
            p0 = mDetectors[i].valleyPos;
        }
        Boolean pvIsTentative = mDetectors[i].mTentative;   // was the peak/valley detected tentative?
        
        // reps found, compare
        if (p1 != -1) {   //
            if ((mDetectState[i].mInterrupted && !pvIsTentative) || // this detector has not detected any cycle yet, or has been interrupted by a cycle. First two reps must be non-tentative.
                (!mDetectState[i].mInterrupted && mPV[i] == mDetectState[i].mLastMatchedPV) ) { // detected cycle, keep matching on the same peak or valley. Skip if already processed this as tentative
                
                // computation optimization, if we incremented before for this PV as a tentative, do not repeat computation
                if (mDetectState[i].mWasTentativelyIncremented && mDetectState[i].mLastRepPos == p0 && !pvIsTentative) {
                    mDetectState[i].mWasTentativelyIncremented = false;    // increment is official
                    continue;
                }

                matchScore = [self dtwX:mCircularBuffer S:p2 E:p1 Y:mCircularBuffer S:p1 E:p0 dimen:-1 compare:NUM_DETECTORS];
                double mrange = [self rangeX:mCircularBuffer S:p2 E:p1 Y:mCircularBuffer S:p1 E:p0 dimen:NUM_DETECTORS];

                double dscore = [self dtwX:mCircularBuffer S:p2 E:p1 Y:mCircularBuffer S:p1 E:p0 dimen:i compare:NUM_DETECTORS];
                double range = [self drangeX:mCircularBuffer S:p2 E:p1 Y:mCircularBuffer S:p1 E:p0 dimen:i];
                
                
                // as reps keeps on increasing, we relax the threshold, to account for form drift when fratigue
                float matchThreshold = MATCH_SCORE_THRESHOLD;
                if (!mDetectState[i].mInterrupted && mDetectState[i].mReps>=2)  // benchpress-4o8-3d needs 0.769 from 3rd rep
                    matchThreshold = 0.08f;
//                if (!mDetectState[i].mInterrupted && mDetectState[i].mReps>=4)  // boxsquart-4io5 needs 0.0801 from 4th rep
//                    matchThreshold = 0.095f;
                // RDLrow-4-insteadof8 rep 8 is 0.0845
                // frontsquat-1short repScore 0.0274  0.0154  0.0172  0.0560
                // frontsquat-1offidentify  repScore 0.0122 (0.0671) 0.0086 0.0883
                // barbelllunge-3io12   0.0417 0.0471  0.06027576
                // bandtriceppushdown-9o8-20d last rep has m=0.0984, not a match
                // frontsquat-0o3-1  rep 3 needs 0.0943
                
                // adaptively threshold, new threshold cannot be too much off from prev average score
                if (!mDetectState[i].mInterrupted)
                    // bandgoodmorning-12o12-15d needs < 2.2 to not count last rep in D6,  barbelllunge-3io12 needs > 2 to count last rep
                    matchThreshold = (float) MIN(2.5 * mDetectState[i].mAvgScore, matchThreshold);
                // but make sure it has a minimum
                matchThreshold = (float) MAX(matchThreshold, MATCH_SCORE_THRESHOLD);

                
                BOOL isMatch = (dscore/range < mDetectorToleranceFactor[i] * DETECTOR_SCORE_THRESHOLD) &&
                    (matchScore / mrange < mMotionToleranceFactor[i] * matchThreshold ||
                     matchScore < MATCH_SCORE_RAW_THRESHOLD) ;  // threshold is constant. Detector has a low distance score, so overall match is good
                if (!isMatch && mDetectState[i].mInterrupted) { // if we have not found a rep yet, check back for similar range movement, maybe PV is detected wrong
                    if ([mDetectors[i] findLastTwoBigPVs:mPV[i]]) {
                        int b1, b2;
                        if (mPV[i]==PEAK) {
                            b2 = mDetectors[i].mLastNBigPeakPos[1];
                            b1 = mDetectors[i].mLastNBigPeakPos[0];
                        }
                        else {
                            b2 = mDetectors[i].mLastNBigValleyPos[1];
                            b1 = mDetectors[i].mLastNBigValleyPos[0];
                        }
                        
                        matchScore = [self dtwX:mCircularBuffer S:b2 E:b1 Y:mCircularBuffer S:b1 E:p0 dimen:-1 compare:NUM_DETECTORS];
                        double mrange = [self rangeX:mCircularBuffer S:b2 E:b1 Y:mCircularBuffer S:b1 E:p0 dimen:NUM_DETECTORS];
                        
                        double dscore = [self dtwX:mCircularBuffer S:b2 E:b1 Y:mCircularBuffer S:b1 E:p0 dimen:i compare:NUM_DETECTORS];
                        double range = [self drangeX:mCircularBuffer S:b2 E:b1 Y:mCircularBuffer S:b1 E:p0 dimen:i];

                        isMatch = (dscore/range < mDetectorToleranceFactor[i] * DETECTOR_SCORE_THRESHOLD) &&
                            (matchScore / mrange < mMotionToleranceFactor[i] * MATCH_SCORE_THRESHOLD ||
                             matchScore < MATCH_SCORE_RAW_THRESHOLD);
                        
                        NSLog(@"BigReps %i mscore %.4f (%.3f/%.3f) at %i %i %i", i, matchScore / mrange, matchScore, mrange, b2, b1, p0);
                        
                        if (isMatch) { // if big threshold works,
                            // have to modify PV too because we are using a bigger threshold, and old PV using default threshold is no longer valid
                            p2 = b2;
                            p1 = b1;
                            mDetectors[i].mLastNPeakPos[0] = mDetectors[i].mLastNBigPeakPos[0];
                            mDetectors[i].mLastNValleyPos[0] = mDetectors[i].mLastNBigValleyPos[0];
                            mDetectors[i].mLastNPeakPos[1] = mDetectors[i].mLastNBigPeakPos[1];
                            mDetectors[i].mLastNValleyPos[1] = mDetectors[i].mLastNBigValleyPos[1];
                            
                            // prevent peak valley saving. A big rep should start a new sequence
                            mDetectState[i].mInterruptPosition = -1;  // highSnatchPullFromFloor-3o4-2d-deadlift with new gravity computation require it
                        }
                    }
                }
                
                if (isMatch) {
                    if (!mDetectState[i].mInterrupted) {   // detected cycle, keeping on matching
                        if (!mDetectState[i].mWasTentativelyIncremented) { // avoid double increment, only increase if this is indeed a new rep
                            mDetectState[i].mAvgScore = (mDetectState[i].mAvgScore * mDetectState[i].mReps + matchScore / mrange) / (mDetectState[i].mReps + 1);
                            mDetectState[i].mAvgDetectorScore = (mDetectState[i].mAvgDetectorScore * mDetectState[i].mReps + dscore / range) / (mDetectState[i].mReps + 1);
                            mDetectState[i].mReps++;
                        }
                        
                        NSLog(@"Continue %i rep %i dscore %.4f (%.3f/%.3f) mscore score %.4f (%.3f/%.3f) at %i %i %i tentative%i", i, mDetectState[i].mReps, dscore / range, dscore, range, matchScore / mrange, matchScore, mrange, p2, p1, p0, pvIsTentative);
                    }
                    // check if last interrupt peak is within range, peak may shift due to projection interrupting early. See RDLrow-4-insteadof8
                    else if (mPV[i] == VALLEY && p1 < mDetectState[i].mInterruptPosition && mDetectState[i].mInterruptPosition < p0) {
                        // last match on peak failed, maybe because we picked the wrong peak position. If the following valley matched, we still consider match, and revert back to peak
                        // triggered by Joshua's RDL+row
                        // powersnatch-0o3-wobbly has problem before a rep is identified, TODO, figure out how to adjust threshold dynamically to get rid of nuance in the peak
                        mPV[i] = PEAK;    // flip back to PEAK matching
                        mDetectState[i].mInt -- ;
                        mDetectState[i].mAvgScore = (mDetectState[i].mAvgScore * mDetectState[i].mReps + matchScore / mrange) / (mDetectState[i].mReps + 1);
                        mDetectState[i].mReps++ ;
                        mDetectState[i].mAvgDetectorScore = (mDetectState[i].mAvgDetectorScore * mDetectState[i].mReps + dscore / range) / (mDetectState[i].mReps + 1) ;
                        p2 = mDetectors[i].mLastNPeakPos[1];
                        pvIsTentative = false;
                        NSLog(@"ValleySavedPeak %i from %i %i %i", i, p2, p1, p0);
                    }
                    else if (mPV[i] == PEAK && p1 < mDetectState[i].mInterruptPosition && mDetectState[i].mInterruptPosition < p0) {
                        // last match on valley failed, maybe because we picked the wrong valley position. If the following peak matched, we still consider match, and revert back to valley
                        mPV[i] = VALLEY;   // flip back to VALLEY matching
                        mDetectState[i].mInt -- ;
                        mDetectState[i].mAvgScore = (mDetectState[i].mAvgScore * mDetectState[i].mReps + matchScore / mrange) / (mDetectState[i].mReps + 1);
                        mDetectState[i].mReps++ ;
                        mDetectState[i].mAvgDetectorScore = (mDetectState[i].mAvgDetectorScore * mDetectState[i].mReps + dscore / range) / (mDetectState[i].mReps + 1);
                        p2 = mDetectors[i].mLastNValleyPos[1];
                        pvIsTentative = false;
                        NSLog(@"PeakSavedValley %i from %i %i %i", i, p2, p1, p0);
                    }
                    else {
                        int delta = p2 - mDetectState[i].mLastRepPos;
                        float prevScore = [self dtwX:mDetectState[i].mWaveform S:0 E:(mDetectState[i].mWaveform)?(mDetectState[i].mWaveformY-1):0
                                              Y:mCircularBuffer S:mDetectors[i].mLastNPeakPos[0] E:mDetectors[i].peakPos dimen:-1 compare:NUM_DETECTORS];
                        float prevRange = [self rangeX:mDetectState[i].mWaveform S:0 E:(mDetectState[i].mWaveform)?(mDetectState[i].mWaveformY-1):0
                                                   Y:mCircularBuffer S:mDetectors[i].mLastNPeakPos[0] E:mDetectors[i].peakPos dimen:NUM_DETECTORS];
                        
                        Boolean newsequence = false;
                        // must guarantee X consecutive reps before allowing breaks
                        if ( ( (delta < INACTIVITY_THRESHOLD && mDetectState[i].mReps >= REP_CONT_THRESHOLD) ||   // do not match if last cycle was too long ago
                              (delta < 10 * INACTIVITY_THRESHOLD && mDetectState[i].mReps > 50) ||  // "long" depends on how far we are into it, do not want to lose progress for long cardio activity
                              // overheadbandpulldown-3o12 requires longer 5 sec break time
                              (delta > 0 && delta < BREAK_THRESHOLD))  // highSnatchPullFromFloor-3o4-2d-deadlift with new gravity computation require delta>0
                            &&
                            (prevScore / prevRange < mMotionToleranceFactor[i] * MATCH_SCORE_THRESHOLD || prevScore < MATCH_SCORE_RAW_THRESHOLD) ) { // pile on only if matched against last sequence
                            mDetectState[i].mAvgScore = (mDetectState[i].mAvgScore * mDetectState[i].mReps + 2 * matchScore / mrange) / (mDetectState[i].mReps + 2);
                            mDetectState[i].mReps += 2;
                            if (p2 < mDetectState[i].mLastRepPos)  // overlap with counted reps, this could happen with BigReps stepping backwards
                                mDetectState[i].mReps--;
                            mDetectState[i].mAvgDetectorScore = (mDetectState[i].mAvgDetectorScore * mDetectState[i].mReps + 2 * dscore / range) / (mDetectState[i].mReps + 2);
                            
                            NSLog(@"PileOn %i rep %i dscore %.4f (%.3f/%.3f) mscore score %.4f (%.3f/%.3f) prev %.4f (%.3f/%.3f) at  %i %i %i tentative%i",
                                  i, mDetectState[i].mReps, dscore / range, dscore, range, matchScore / mrange, matchScore, mrange, prevScore / prevRange, prevScore, prevRange,
                                  p2, p1, p0, pvIsTentative);
                        } else {   // restart a new sequence
                            // TODO, send a notification if last sequence was pretty long, prevent accidental lose if user forgot to save
                            
                            // we started a new sequence, do not lose save state for last sequence, allocate a new data structure
                            if (i==mSaveDetector)
                                mDetectState[i] = [[DetectState alloc] init];
                            
                            mDetectState[i].mReps = 2;
                            mDetectState[i].mAvgScore = matchScore / mrange;
                            mDetectState[i].mAvgDetectorScore = dscore/range;
                            mDetectState[i].mBestScore = matchScore / mrange ;
                            mDetectState[i].mInt = 0;
                            newsequence = true;
                            
                            NSLog(@"Start %i rep %i dscore %.4f (%.3f/%.3f) mscore score %.4f (%.3f/%.3f) prev %.4f (%.3f/%.3f) at  %i %i %i tentative%i",
                                  i, mDetectState[i].mReps, dscore / range, dscore, range, matchScore / mrange, matchScore, mrange, prevScore / prevRange, prevScore, prevRange,
                                  p2, p1, p0, pvIsTentative);
                        }
                        
                        // the range of last two reps if we were to back track another half cycle
                        int o2, o1, o0;
                        int halfCycleLength = abs(mDetectors[i].mLastNValleyPos[0] - mDetectors[i].mLastNPeakPos[0]);
                        if (mPV[i]==VALLEY) {
                            o2 = mDetectors[i].mLastNValleyPos[1] - halfCycleLength;
                            o1 = mDetectors[i].mLastNPeakPos[0];
                            o0 = mDetectors[i].peakPos;
                        }
                        else {  // must be peak
                            o2 = mDetectors[i].mLastNPeakPos[1] - halfCycleLength;
                            o1 = mDetectors[i].mLastNValleyPos[0];
                            o0 = mDetectors[i].valleyPos;
                        }
                        
                        // we have identified a rep, so we know the rep range, go back search Peak/Valley based on the new threshold
                        [mBacktrackDetector reset];
                        // setup threshold based on detected first rep's range
                        mBacktrackDetector.peakVal = mDetectors[i].mLastNPeakVal[0];
                        mBacktrackDetector.valleyVal = mDetectors[i].mLastNValleyVal[0];
                        mBacktrackDetector.peakPos = p2 - mDetectors[i].mLastNPeakPos[0];
                        mBacktrackDetector.valleyPos = p2 - mDetectors[i].mLastNValleyPos[0];
                        mBacktrackDetector.mLastNPeakPos[0] = p2 - mDetectors[i].peakPos;
                        mBacktrackDetector.mLastNValleyPos[0] = p2 - mDetectors[i].valleyPos;
                        mBacktrackDetector.thresholdPercent = mDetectors[i].thresholdPercent;
                        mBacktrackDetector.threshold = mDetectors[i].threshold;   // used after timeout
                        mBacktrackDetector.mDynamicThreshold = true;
                        mBacktrackDetector.mDynamicThresholdNoLimit = true;
                        mBacktrackDetector.mThresholdTimeout = false;  // no dynamic threshold timeout, we want to use the same range of rep to backtrack
                        
                        Boolean backtrackTentativelyIncremented = false;
                        int backtrackStartPosition = p2;
                        // go back 1 more than lastRepPos in order to identify lastRepPos as PV too
                        NSLog(@"ExploreBack %i start %i upto %i", MIN(backtrackStartPosition - mDetectState[i].mLastRepPos+1, 20*SAMPLE_RATE), backtrackStartPosition, mDetectState[i].mLastRepPos);
                        for (int k=0 ; k<=MIN(backtrackStartPosition - mDetectState[i].mLastRepPos+1, 20*SAMPLE_RATE) ; k++) { // go back at most 20 seconds, or until where last rep detected
                            if (backtrackStartPosition-k<0) break;   // maybe -1 if LastRepPos is 0 and we go back 1 more
                            
                            int pv = [mBacktrackDetector postNewSample:mCircularBuffer[i][(backtrackStartPosition-k)%BUFFER_SIZE] at:timestamp - mBufferTimestamp[(backtrackStartPosition-k)%BUFFER_SIZE] ];
                            if (pv == mPV[i]) { // we found a previous matching PV
                                if (pv == PEAK)
                                    p3 = backtrackStartPosition - mBacktrackDetector.peakPos + 1;
                                else
                                    p3 = backtrackStartPosition - mBacktrackDetector.valleyPos + 1;
                                
                                if (p3 == backtrackStartPosition) continue;   // the first one may be picked up, because it is a peak or valley by definition
                                
                                // computation optimization, do not recompute when it is final
                                if (backtrackTentativelyIncremented && !mBacktrackDetector.mTentative) {
                                    backtrackTentativelyIncremented = false;    // increment is official
                                    continue;
                                }
                                
                                float backScore = [self dtwX:mCircularBuffer S:p3 E:p2 Y:mCircularBuffer S:p2 E:p1 dimen:-1 compare:NUM_DETECTORS];
                                float backRange = [self rangeX:mCircularBuffer S:p3 E:p2 Y:mCircularBuffer S:p2 E:p1 dimen:NUM_DETECTORS];
                                
                                float backDetectorScore = [self dtwX:mCircularBuffer S:p3 E:p2 Y:mCircularBuffer S:p2 E:p1 dimen:i compare:NUM_DETECTORS];
                                float backDetectorRange = [self drangeX:mCircularBuffer S:p3 E:p2 Y:mCircularBuffer S:p2 E:p1 dimen:i];
                                
                                if ( (backDetectorScore/backDetectorRange < DETECTOR_SCORE_THRESHOLD) &&
                                    // allow more room when going back, detector 6 etc. has trouble to get started
                                    // closegripbench-6o8-7d needs 0.11f threshold for detector 1
                                    (backScore / backRange < 0.07f || backScore<MATCH_SCORE_RAW_THRESHOLD)) {
                                    if (!backtrackTentativelyIncremented) {
                                        p0 = p1;  // shift detected rep positions
                                        p1 = p2;
                                        p2 = p3;
                                        mDetectState[i].mAvgScore = (mDetectState[i].mAvgScore * mDetectState[i].mReps + backScore / backRange) / (mDetectState[i].mReps + 1);
                                        mDetectState[i].mAvgDetectorScore = (mDetectState[i].mAvgDetectorScore * mDetectState[i].mReps + backDetectorScore / backDetectorRange) / (mDetectState[i].mReps + 1) ;
                                        mDetectState[i].mReps++;
                                        
                                        // update the half cycle range
                                        o0 = o1;
                                        // o2 was an estimated position of opposite PV, check if we can find better
                                        if (pv == PEAK && mBacktrackDetector.valleyPos != -1) {
                                            o1 = backtrackStartPosition - mBacktrackDetector.valleyPos;
                                            halfCycleLength = mBacktrackDetector.valleyPos - mBacktrackDetector.mLastNPeakPos[0];
                                            if (mBacktrackDetector.mLastNPeakPos[0] == -1)   // this is the first cycle backtrack, starting position is the last peak
                                                halfCycleLength--;
                                        } else if (pv == VALLEY && mBacktrackDetector.peakPos != -1) {
                                            o1 = backtrackStartPosition - mBacktrackDetector.peakPos;
                                            halfCycleLength = mBacktrackDetector.peakPos - mBacktrackDetector.mLastNValleyPos[0];
                                            if (mBacktrackDetector.mLastNValleyPos[0] == -1)   // this is the first cycle backtrack, starting position is the last valley
                                                halfCycleLength--;
                                        } else
                                            o1 = o2;
                                        o2 = p2 - halfCycleLength;
                                        
                                        NSLog(@"Back %i rep %i dscore %.4f (%.3f/%.3f) mscore score %.4f (%.3f/%.3f) at  %i %i %i",
                                              i, mDetectState[i].mReps, backDetectorScore / backDetectorRange, backDetectorScore, backDetectorRange, backScore / backRange, backScore, backRange,
                                              p2, p1, p0);
                                    }
                                    backtrackTentativelyIncremented = mBacktrackDetector.mTentative;
                                }
                                else {
                                    NSLog(@"BackFail %i rep %i dscore %.4f (%.3f/%.3f) mscore score %.4f (%.3f/%.3f) at  %i %i %i",
                                          i, mDetectState[i].mReps, backDetectorScore / backDetectorRange, backDetectorScore, backDetectorRange, backScore / backRange, backScore, backRange,
                                          p3, p2, p1);
                                    break;    // no need to keep going back if we did not match the previous rep
                                }
                            }
                        }
                        
                        // back track half a cycle further to see if we mis-identified the last opposite PV. We have trouble identifying the beginning PV for a new sequence
                        // TODO we currently backtrack by time because it may be hard to identify start of PV, revisit to see if we should use the PV detector, maybe to find the last tentative PV
                        if (o2>0 && o2>=mDetectState[i].mLastRepPos) {  // make sure do not double count, happens when PileOn and backing off
                            double backtrackScore = [self dtwX:mCircularBuffer S:o2 E:o1 Y:mCircularBuffer S:o1 E:o0 dimen:-1 compare:NUM_DETECTORS];
                            double backtrackRange = [self rangeX:mCircularBuffer S:o2 E:o1 Y:mCircularBuffer S:o1 E:o0 dimen:NUM_DETECTORS];
                            
                            float backtrackDetectorScore = [self dtwX:mCircularBuffer S:o2 E:o1 Y:mCircularBuffer S:o1 E:o0 dimen:i compare:NUM_DETECTORS];
                            double backtrackDetectorRange = [self drangeX:mCircularBuffer S:o2 E:o1 Y:mCircularBuffer S:o1 E:o0 dimen:i];
                            
                            // be generous when backtracking
                            // pullup-3o6 needs detector 0.097
                            if ( backtrackDetectorScore/backtrackDetectorRange < 1.3*DETECTOR_SCORE_THRESHOLD &&
                                // bandpullapart-11o12 needs 0.09f
                                (backtrackScore / backtrackRange < 0.07f ||
                                 backtrackScore < 1.2 * MATCH_SCORE_RAW_THRESHOLD)) {   // backtrack success, mark it as first rep
                                    NSLog(@"BackHalf %i dscore %.4f (%.3f/%.3f) mscore score %.4f (%.3f/%.3f) from %i %i %i to %i %i %i",
                                          i, backtrackDetectorScore / backtrackDetectorRange, backtrackDetectorScore, backtrackDetectorRange, backtrackScore / backtrackRange, backtrackScore, backtrackRange,
                                          p2, p1, p0, o2, o1, o0);
                                    
                                    mDetectState[i].mAvgScore += (backtrackScore/backtrackRange - matchScore / mrange) / mDetectState[i].mReps;
                                    mDetectState[i].mAvgDetectorScore += (backtrackDetectorScore/backtrackDetectorRange - dscore/range) / mDetectState[i].mReps;
                                    
                                    p2 = o2;
                                    p0 = o0;
                                    pvIsTentative = false;  // switched half cycle, this cannot be tentative anymore
                                    if (mPV[i] == PEAK)     // flip half a cycle
                                        mPV[i] = VALLEY;
                                    else
                                        mPV[i] = PEAK;
                                } else
                                    NSLog(@"BackHalfFail %i dscore %.4f (%.3f/%.3f) mscore score %.4f (%.3f/%.3f) from %i %i %i to %i %i %i",
                                          i, backtrackDetectorScore / backtrackDetectorRange, backtrackDetectorScore, backtrackDetectorRange, backtrackScore / backtrackRange, backtrackScore, backtrackRange,
                                          p2, p1, p0, o2, o1, o0);
                        }
                        
                        if (newsequence) {
                            mDetectState[i].mStartTime = [NSDate timeIntervalSinceReferenceDate]*1000 - (timestamp - mBufferTimestamp[p2 % BUFFER_SIZE]);  // find the beginning of the first rep. In milliseconds
                            mDetectState[i].mStartPosition = p2;
                            mDetectState[i].mInt = 0;
                        }
                    }
                    mDetectState[i].mWasTentativelyIncremented = pvIsTentative;  // if this is tentative PV, this is tentative increase
                    
                    mDetectState[i].mLastRepLength = p0-p1;  // used to determine if we idled for too long
                    mDetectState[i].mInterrupted = false;
                    mDetectState[i].mLastRepPos = p0;     // record end of last rep to know how long we waited before next rep comes
                    mDetectState[i].mLastMatchedPV = mPV[i];    // record whether we are matching on Peak or Valley
                    mDetectState[i].mRange = (float) range;
                    
                    mDetectors[i].mDynamicThreshold = true;  // the threshold is now a function of the current range
                    mDetectors[i].mDynamicThresholdNoLimit = false;  // initially still limited by a minimum, open up later
                    if (mDetectState[i].mReps >= REP_REPORT_THRESHOLD)
                        mDetectors[i].mDynamicThresholdNoLimit = true;  // threshold can be as small as possible, as long as it is bigger than a percentage of the previous rep
                    
                    // save waveform if needed
                    if (matchScore / mrange <= mDetectState[i].mBestScore) {   // save waveform if this is better looking
                        mDetectState[i].mBestScore = matchScore / mrange ;
                        // waveform to save
                        int len = mDetectors[i].peakPos - mDetectors[i].mLastNPeakPos[0] + 1;
                        [MotionDetect deallocFloatArray:mDetectState[i].mWaveform];   // free up memory if allocated before
                        mDetectState[i].mWaveform = [MotionDetect allocFloatArrayX:SAVE_DIMENSION Y:len];
                        mDetectState[i].mWaveformX = SAVE_DIMENSION;
                        mDetectState[i].mWaveformY = len;
                        for (int j = 0; j < len; j++) {
                            for (int k = 0; k < SAVE_DIMENSION; k++)
                                mDetectState[i].mWaveform[k][j] = mCircularBuffer[k][(j + mDetectors[i].mLastNPeakPos[0]) % BUFFER_SIZE];
                        }
                        
                        NSLog(@"UpdateWaveform %i %i %i %i %i", i, mDetectors[i].mLastNPeakPos[0], mDetectors[i].peakPos, mDetectors[i].mLastNValleyPos[0], mDetectors[i].valleyPos);
                    }
                    
                    // should this become the new detector
                    // bandgoodmorning-12o12-15d d3's extra rep comes after good detector ends, need to check to make sure it does not overwrite good detector
                    if (mCurrentDetector != i) {
                        int compareDetector = mCurrentDetector;
                        if (compareDetector == -1 && mSaveDetector != -1 && mDetectState[mSaveDetector].mLastRepPos > p2)  // use save detector even if it is interruptted, if last rep overlap our reps
                            compareDetector = mSaveDetector;
                        if (mDetectState[i].mReps >= REP_REPORT_THRESHOLD && (compareDetector == -1 || compareDetector == i)) {      // if no detector, just replace. Or if me again, just proceed
                            //                                if ( mSaveState==null || p2 > mSaveState.mLastRepPos || mDetectState[i].mAvgDetectorScore+mDetectState[i].mAvgScore < mSaveState.mAvgDetectorScore+mSaveState.mAvgScore )
                            mCurrentDetector = i;
                            
                            // a new motion detected, if SaveState has a different motion, we may need to save that first
                            // unless the new rep overlaps with last detected motion (i.e., piling on to old motion), attempts to log
                            if (! (mSaveState!=nil && mSaveState.mLastRepPos > mDetectState[i].mStartPosition))
                                [self.delegate logExercise];
                            
                            mSaveDetector = i; // if we were to save, we will save this detector
                            mSaveState = mDetectState[i];
                            
//                            if (mMotionMatcher != null)
//                                mMotionMatcher.cancel(true);   // cancel last match because we found a better detector
//                            mMotionMatcher = new MatchMotion();
//                            mMotionMatcher.execute(mCurrentDetector);  // find out what motion it is when it becomes the active detector. Side effect, will update current detector's matching exercise name
                            
                            // find out what motion it is when it becomes the active detector. Side effect, will update current
                            [self matchMotion:mCurrentDetector];
                            NSLog(@"NewDetector %i", i);
                        } else if (mDetectState[i].mReps >= REP_REPORT_THRESHOLD) {    // check if new detector is better, is it worthy to replace the old
                            //                                double newQuality = mDetectState[i].mScore / mDetectState[i].mReps;
                            //                                double oldQuality = mDetectState[mCurrentDetector].mScore / mDetectState[mCurrentDetector].mReps;
                            if ((mDetectState[compareDetector].mLastMatchedPV == VALLEY &&
                                 mDetectors[compareDetector].lastindex - mDetectors[compareDetector].valleyPos > INACTIVITY_THRESHOLD) ||   // replace if current detector has been stale for a while
                                
                                (mDetectState[compareDetector].mLastMatchedPV == PEAK &&
                                 mDetectors[compareDetector].lastindex - mDetectors[compareDetector].peakPos > INACTIVITY_THRESHOLD) ||
                                
                                // when both detectors are good, use range as criteria
                                (mDetectState[i].mAvgScore < GOOD_SCORE_THRESHOLD &&
                                 mDetectState[compareDetector].mAvgScore < GOOD_SCORE_THRESHOLD &&
                                 0.9 * mDetectState[i].mRange / mRangeReductionFactor[i] > mDetectState[compareDetector].mRange / mRangeReductionFactor[compareDetector]) ||
                                
                                //                                        mDetectState[i].mReps / (mDetectState[i].mInt + 1) > 1.5 * mDetectState[compareDetector].mReps / (mDetectState[compareDetector].mInt + 1) ||    // replace if current detector has longer streak per interruption
                                // RDLrow-4-insteadof8 detector 2 less range, but better quality. 6,7 detectors detect double reps
                                ((mDetectState[i].mAvgScore >= GOOD_SCORE_THRESHOLD ||
                                  mDetectState[compareDetector].mAvgScore >= GOOD_SCORE_THRESHOLD) &&
                                 (mDetectState[i].mAvgScore / mDetectState[i].mRange * mRangeReductionFactor[i])
                                 // existing_pullup_pullup_5_3 has bad detector, but great range, motivating us to use ratio
                                 // but RDLrow-4-insteadof8  has good detector, but smaller range
                                 < 0.9 * (mDetectState[compareDetector].mAvgScore / mDetectState[compareDetector].mRange * mRangeReductionFactor[compareDetector]))) {
                                    
                                    
                                    //                                        ||  // replace if detector has a higher quality, e.g., 80% better
                                    //                                        // care only about detector quality comparison, because we are choosing detector. barbelllunge-3io12 switch to 7 because motion match is better, but is detector match is weak
                                    //
                                    //                                        ( (mDetectState[i].mAvgDetectorScore + mDetectState[i].mAvgScore < 1.2 * (mDetectState[compareDetector].mAvgDetectorScore + mDetectState[compareDetector].mAvgScore)) && (
                                    //                                            (compareDetector>=6 && i<=3 && mDetectState[i].mRange > 0.8 * mDetectState[compareDetector].mRange )  ||      // replace if it is a more reliable detector
                                    //
                                    //                                            mDetectState[i].mRange > 2.5 * mDetectState[compareDetector].mRange)  )) { // new stronger detector found, take more magnitude to overtake an incumbent detector
                                    
                                    if (mDetectState[compareDetector].mLastMatchedPV == VALLEY &&
                                        mDetectors[compareDetector].lastindex - mDetectors[compareDetector].valleyPos > INACTIVITY_THRESHOLD)
                                            NSLog(@"becauseValleyTimeout %i index %i valley %i peak %i", mDetectState[compareDetector].mLastMatchedPV,
                                                  mDetectors[compareDetector].lastindex, mDetectors[compareDetector].valleyPos, mDetectors[compareDetector].peakPos);
                                    else if (mDetectState[compareDetector].mLastMatchedPV == PEAK &&
                                                 mDetectors[compareDetector].lastindex - mDetectors[compareDetector].peakPos > INACTIVITY_THRESHOLD)
                                            NSLog(@"becausePeakTimeout %i index %i valley %i peak %i", mDetectState[compareDetector].mLastMatchedPV,
                                                  mDetectors[compareDetector].lastindex, mDetectors[compareDetector].valleyPos, mDetectors[compareDetector].peakPos);
                                    else if ((mDetectState[i].mAvgDetectorScore + mDetectState[i].mAvgScore) < GOOD_SCORE_THRESHOLD &&
                                                     (mDetectState[compareDetector].mAvgDetectorScore + mDetectState[compareDetector].mAvgScore) < GOOD_SCORE_THRESHOLD &&
                                                     0.9 * mDetectState[i].mRange > mDetectState[compareDetector].mRange)
                                            NSLog(@"becauseBetterRange %i index %i valley %i peak %i quality %f %f %f %f %f %f", mDetectState[compareDetector].mLastMatchedPV,
                                                  mDetectors[compareDetector].lastindex, mDetectors[compareDetector].valleyPos, mDetectors[compareDetector].peakPos,
                                                  mDetectState[compareDetector].mAvgDetectorScore, mDetectState[i].mAvgDetectorScore,
                                                  mDetectState[compareDetector].mAvgScore, mDetectState[i].mAvgScore,
                                                  mDetectState[compareDetector].mRange, mDetectState[i].mRange);
                                    else  // if (mDetectState[i].mAvgDetectorScore + mDetectState[i].mAvgScore < 0.8 * (mDetectState[compareDetector].mAvgDetectorScore + mDetectState[compareDetector].mAvgScore))
                                            NSLog(@"becauseHigherQuality %i index %i valley %i peak %i quality %f %f %f %f %f %f", mDetectState[compareDetector].mLastMatchedPV,
                                                  mDetectors[compareDetector].lastindex, mDetectors[compareDetector].valleyPos, mDetectors[compareDetector].peakPos,
                                                  mDetectState[compareDetector].mAvgDetectorScore, mDetectState[i].mAvgDetectorScore,
                                                  mDetectState[compareDetector].mAvgScore, mDetectState[i].mAvgScore,
                                                  mDetectState[compareDetector].mRange, mDetectState[i].mRange);
                                    
                                    NSLog(@"ReplaceDetector %i w %i range %.3f %.3f quality %.3f %.3f %.3f %.3f int %i %i", compareDetector, i, mDetectState[compareDetector].mRange, mDetectState[i].mRange, mDetectState[compareDetector].mAvgDetectorScore, mDetectState[i].mAvgDetectorScore, mDetectState[compareDetector].mAvgScore, mDetectState[i].mAvgScore, mDetectState[compareDetector].mInt, mDetectState[i].mInt);
                                    mCurrentDetector = i;
                                    
                                    // a new motion detected, if SaveState has a different motion, we may need to save that first
                                    // unless the new rep overlaps with last detected motion (i.e., piling on to old motion), attempts to log
                                    if (! (mSaveState!=nil && mSaveState.mLastRepPos > mDetectState[i].mStartPosition))
                                        [self.delegate logExercise];
                        
                                    mSaveDetector = i; // if we were to save, we will save this detector
                                    mSaveState = mDetectState[i];
//                                    
//                                    if (mMotionMatcher != null)
//                                        mMotionMatcher.cancel(true);   // cancel last match because we found a better detector
//                                    mMotionMatcher = new MatchMotion();
//                                    mMotionMatcher.execute(mCurrentDetector);  // find out what motion it is when it becomes the active detector. Side effect, will update current detector's matching exercise name
                                    // find out what motion it is when it becomes the active detector. Side effect, will update current detector's matching exercise name
                                    [self matchMotion:mCurrentDetector];
                            } else
                                NSLog(@"StayDetector %i w %i range %.3f %.3f quality %.3f %.3f %.3f %.3f int %i %i", compareDetector, i, mDetectState[compareDetector].mRange, mDetectState[i].mRange, mDetectState[compareDetector].mAvgDetectorScore, mDetectState[i].mAvgDetectorScore, mDetectState[compareDetector].mAvgScore, mDetectState[i].mAvgScore, mDetectState[compareDetector].mInt, mDetectState[i].mInt);
                        
                        }
                    }
                    
                    if ( i==mCurrentDetector ) {  // found a new rep, so if we are the detector, we need to report reps
                        [self.delegate reportRep:mDetectState[mCurrentDetector].mReps wDetector:mCurrentDetector];
                    }
                    
                } else if (!pvIsTentative || p0 - p1 > REP_COMP_DIFF_FACTOR * (p1 - p2)) {  // rep comparison did not match, only take action if PV is not tentative. Or if it has been a while, to trigger projection
                    // bandpullapart-11o12 does not terminate properly, force to use projection
                    // TODO, what to do with temp increases, do we backtrack? It is going to be interrupted anyway, maybe should just leave it as is
                    //                            if (mDetectState[i].mWasTentativelyIncremented)
                    //                                mDetectState[i].mReps --;
                    if (!mDetectState[i].mInterrupted) {  // if first time, mark it
                        
                        // project forward, in case, we did not find clear marker for end of rep
                        // only project if we did not tentative increase, otherwise, it would cause duplicate
                        if (!mDetectState[i].mWasTentativelyIncremented && p0 - p1 > p1 - p2) {
                            int projectSpot = p1 + (p1 - p2);
                            double forwardMatchScore = [self dtwX:mCircularBuffer S:p2 E:p1 Y:mCircularBuffer S:p1 E:projectSpot dimen:-1 compare:NUM_DETECTORS];
                            double forwardMrange = [self rangeX:mCircularBuffer S:p2 E:p1 Y:mCircularBuffer S:p1 E:projectSpot dimen:NUM_DETECTORS];
                            
                            double forwardDscore = [self dtwX:mCircularBuffer S:p2 E:p1 Y:mCircularBuffer S:p1 E:projectSpot dimen:i compare:NUM_DETECTORS];
                            double forwardRange = [self drangeX:mCircularBuffer S:p2 E:p1 Y:mCircularBuffer S:p1 E:projectSpot dimen:i];

                            if (forwardDscore / forwardRange < mDetectorToleranceFactor[i] * DETECTOR_SCORE_THRESHOLD &&
                                (forwardMatchScore / forwardMrange < mMotionToleranceFactor[i] * MATCH_SCORE_THRESHOLD ||
                                 forwardMatchScore < MATCH_SCORE_RAW_THRESHOLD)) {
                                mDetectState[i].mReps++;
                                mDetectState[i].mLastRepPos = projectSpot;
                                NSLog(@"Projection %i rep %i dscore %.4f (%.3f/%.3f) mscore score %.4f (%.3f/%.3f) at  %i %i %i",
                                      i, mDetectState[i].mReps, forwardDscore / forwardRange, forwardDscore, forwardRange, forwardMatchScore / forwardMrange, forwardMatchScore, forwardMrange,
                                      p2, p1, projectSpot);
                                
                                if (i==mCurrentDetector)
                                    [self.delegate reportRep:mDetectState[i].mReps wDetector:i];
                            }
                            else
                                NSLog(@"ProjectionFail %i rep %i dscore %.4f (%.3f/%.3f) mscore score %.4f (%.3f/%.3f) at  %i %i %i",
                                      i, mDetectState[i].mReps, forwardDscore / forwardRange, forwardDscore, forwardRange, forwardMatchScore / forwardMrange, forwardMatchScore, forwardMrange,
                                      p2, p1, projectSpot);
                        }
                        
                        
                        mDetectState[i].mInterrupted = true;
                        mDetectState[i].mInterruptPosition = p0;
                        mDetectState[i].mInt++;
                        mDetectors[i].mDynamicThreshold = false;  // the threshold is back to the minimum as we are looking for a new sequence
                        
                        if (i == mCurrentDetector) { // only reset if we are the current detector
                            mCurrentDetector = -1;
                            NSLog(@"ClearDetector %i rep %i", i, mDetectState[i].mReps);
                        }
                        NSLog(@"Interrupt %i rep %i dscore %.4f (%.3f/%.3f) mscore score %.4f (%.3f/%.3f) at  %i %i %i",
                              i, mDetectState[i].mReps, dscore / range, dscore, range, matchScore / mrange, matchScore, mrange,
                              p2, p1, p0);
                        
                        mDetectState[i].mWasTentativelyIncremented = false;
                    } else
                        NSLog(@"Nomatch %i rep %i dscore %.4f (%.3f/%.3f) mscore score %.4f (%.3f/%.3f) at  %i %i %i",
                              i, mDetectState[i].mReps, dscore / range, dscore, range, matchScore / mrange, matchScore, mrange,
                              p2, p1, p0);
                } else
                    NSLog(@"NomatchTentative %i rep %i dscore %.4f (%.3f/%.3f) mscore score %.4f (%.3f/%.3f) at  %i %i %i tentative",
                          i, mDetectState[i].mReps, dscore / range, dscore, range, matchScore / mrange, matchScore, mrange,
                          p2, p1, p0);
            }
        }
        
    }
    
    
}


- (void) matchMotion:(int) detector {
    NSMutableArray * array = [[NSMutableArray alloc] init];
    _currentMatchNum ++ ;  // older match threads will see this and terminate
    int myMatchNum = _currentMatchNum;  // number will not change inside the matching block
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // compute match
        for (Motion * motion in [self.motionLibrary allValues]) {
            float minScore = 1000, minRange = 1;
            for (int i = 0; i < NUM_SAVED_MOTION; i++) {
                
                if (motion.mBuffer[i] == nil) continue;  // skip no motion exercises
                if (motion.mDetector[i] == -1) continue; // skip if no detector recorded, should not happen, something else if wrong
                
                int xb = 0, xe = 0;
                // use motion's detector instead of currentdetector. If this motion should match, it should have PV in detector's dimension
                // because of time delay of ZAC and LIN, cannot use currentDetector
                if (mDetectors[ motion.mDetector[i] ].mLastNPeakPos[0] != -1) {
                    xb = mDetectors[ motion.mDetector[i] ].mLastNPeakPos[0];
                    xe = mDetectors[ motion.mDetector[i] ].peakPos;
                }
                
                float score = [self dtwX:mCircularBuffer S:xb E:xe Y:motion.mBuffer[i] S:0 E:motion.mBufferY[i]-1 dimen:-1 compare:NUM_DETECTORS];
                float range = [self rangeX:mCircularBuffer S:xb E:xe Y:motion.mBuffer[i] S:0 E:motion.mBufferY[i]-1 dimen:NUM_DETECTORS];

                if (score / range < minScore/minRange) {
                    minScore = score;
                    minRange = range;
                }
                
                NSLog(@"................onematch %@ mscore %.4f (%.3f/%.3f) from %i %i", motion.mName, score / range, score, range, xb, xe);
            }
            if (minScore != 1000) {
                [array addObject:@{@"name":motion.mName, @"score":@(minScore), @"range":@(minRange), @"ratio":@(minScore/minRange) }];
            }
            
            if (myMatchNum < _currentMatchNum) {
                NSLog(@"MatchCancelled %i", detector);
                return;
            }
        
        }
        

        
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"ratio" ascending:YES];
        NSArray * sortedArray=[array sortedArrayUsingDescriptors:@[sort]];
        
//        logProgress("matched " + entry.getKey() + " " + entry.getValue().score/entry.getValue().range + " " + entry.getValue().score + "/" + entry.getValue().range);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            mDetectedExerciseName = nil;
            mDetectedExercises = sortedArray;
            if (sortedArray.count > 0) {
                NSDictionary * entry = [sortedArray objectAtIndex:0];
                if ([[entry objectForKey:@"ratio"] floatValue] < MATCH_FUZZ_FACTOR * MATCH_SCORE_THRESHOLD ||
                    [[entry objectForKey:@"score"] floatValue] < MATCH_FUZZ_FACTOR * MATCH_SCORE_RAW_THRESHOLD
                    ) {      // more lenient threshold for detecting motion, because your next movement may not be as close as before, because of watch position etc.

                    mDetectedExerciseName = [entry objectForKey:@"name"];
                    if (mDetectedExerciseName!=nil)
                        [self.delegate reportDetectedExercise: mDetectedExerciseName ];
                }
            }
        });
    });
    

}


- (float) drangeX:(float**)x S:(int) xb E:(int) xe Y:(float**)y S:(int) yb E:(int) ye dimen:(int)d {
    if (x==nil) return 0.000001f;   // return a very small range so that we do not match
    
    float min = INFINITY;
    float max = -INFINITY;
    float range = 0;
    for (int i = xb; i <= xe; i++) {
        if (x[d][i % BUFFER_SIZE] < min)
            min = x[d][i % BUFFER_SIZE];
        if (x[d][i % BUFFER_SIZE] > max)
            max = x[d][i % BUFFER_SIZE];
    }
    range += max-min;
    min = INFINITY;
    max = -INFINITY;
    for (int i = yb; i <= ye; i++) {
        if (y[d][i % BUFFER_SIZE] < min)
            min = y[d][i % BUFFER_SIZE];
        if (y[d][i % BUFFER_SIZE] > max)
            max = y[d][i % BUFFER_SIZE];
    }
    range += max-min;
    
    return range;
}

// find range of all dimensions
- (float) rangeX:(float**)x S:(int) xb E:(int) xe Y:(float**)y S:(int) yb E:(int) ye dimen:(int) compareDimen {
    if (x==nil) return 0.000001f;   // return a very small range so that we do not match
 
    // Find min max range for all dimensions
    float minmax[10][4]; // minx, maxx, miny, maxy for all 6 dimensions
    for (int j = 0; j < compareDimen; j++) {
        minmax[j][0] = INFINITY;
        minmax[j][1] = -INFINITY;
        minmax[j][2] = INFINITY;
        minmax[j][3] = -INFINITY;
    }
    float range = 0;
    for (int j = 0; j < compareDimen; j++) {
        if (j != 3 && j != 4 && j != 5) {    // skip gravity dimensions
            for (int i = xb; i <= xe; i++) {
                if (x[j][i % BUFFER_SIZE] < minmax[j][0])
                    minmax[j][0] = x[j][i % BUFFER_SIZE];
                if (x[j][i % BUFFER_SIZE] > minmax[j][1])
                    minmax[j][1] = x[j][i % BUFFER_SIZE];
            }
            range += minmax[j][1] - minmax[j][0];
            for (int i = yb; i <= ye; i++) {
                if (y[j][i % BUFFER_SIZE] < minmax[j][2])
                    minmax[j][2] = y[j][i % BUFFER_SIZE];
                if (y[j][i % BUFFER_SIZE] > minmax[j][3])
                    minmax[j][3] = y[j][i % BUFFER_SIZE];
            }
            range += minmax[j][3] - minmax[j][2];
        }
    }
    
    return range + 60;   // fixed range for gravity dimensions
}

// dynamic time warping for all six dimensions
- (float) dtwX:(float**) x S:(int) xb E:(int) xe Y:(float**) y S:(int) yb E:(int) ye  dimen:(int)dimen compare:(int) compareDimen {
    if (xb == xe)
        return 10000;  // return infinite if first sequence is invalid. Happens the first time we find a valid sequence and there is no prior exercise to match against
    
    // adjust for wrap around
    if (xe < xb)
        xe += BUFFER_SIZE;
    if (ye < yb)
        ye += BUFFER_SIZE;
    int xl = xe - xb + 1;
    int yl = ye - yb + 1;
    float cost;
    
    // swap x & y to make Y longer, to simplify the logic below. Not doing for XE,YE because we do not use it below
    if (yl < xl) {
        float** tmp = x;
        x = y;
        y = tmp;
        int t = xb;
        xb = yb;
        yb = t;
        t = xe;
        xe = ye;
        ye = t;
        t = xl;
        xl = yl;
        yl = t;
    }
    
    if (yl > INACTIVITY_THRESHOLD)
        return 20000;  // do not match if the rep is too long, save computation time
    if (yl > REP_COMP_DIFF_FACTOR * xl) return 30000;  // do not match if scale is way off,
    if (yl-xl > REP_TIME_DIFF) return 40000;  // cannot be too different, this is to save computation time
    
    // declare DTW matrix. Has one more dimension to account for boundary condition (set to 0)
    float distance[xl + 1][xl + 1];
    
    // 10% window, TODO adjust threshold
    int w = lroundf(xl * 0.2f);   // enlarge to 20%, but have not seen a case where this might help, a precaution
    
    // initialize all cells to be infinity to prevent people from using it
    for (int i = 0; i <= xl; i++)
        for (int j = 0; j <= xl; j++)
            distance[i][j] = 10000000; // really big number to prevent match go beyond band
    distance[0][0] = 0;
    
    for (int i = 1; i <= xl; i++) {
        for (int j = MAX(1, i - w); j <= MIN(xl, i + w); j++) {
            // compute current point cost.
            // cost matrix is 1-based, must convert to 0-based to access the array
            if (dimen == -1) {         // means all 6 dimensions together
                cost = 0;
                //                    int dToCompare = 6;
                //                    if (matchLib)   // if comparing with library to detect which one is match, need to compare Acc dimensions too
                //                        dToCompare = 8;
                for (int k = 0; k < compareDimen ; k++) {
                    double znormalize = 0;
                    if (k == 0 || k == 1 || k == 2 || k == 6 || k == 7 || k == 8 || k == 9)
                        znormalize = (y[k][yb % BUFFER_SIZE] - x[k][xb % BUFFER_SIZE] + y[k][ye % BUFFER_SIZE] - x[k][xe % BUFFER_SIZE]) / 2;
                    
                    // scale Y to get the actual value that should be at position j
                    float idealPos = (float)(j-1) * (float)(yl-1) / (float)(xl-1);
                    int idealPosInt = (int) floor(idealPos);
                    float backPortion = idealPos - idealPosInt;
                    float ysample = y[k][(yb+idealPosInt)%BUFFER_SIZE] * (1-backPortion);
                    if (yb+idealPosInt+1<=ye)
                        ysample += y[k][(yb+idealPosInt+1)%BUFFER_SIZE] * backPortion;
                    double onecost = fabs(x[k][(i - 1 + xb) % BUFFER_SIZE] - ysample + znormalize);
                    if (k == 7 || k == 8 || k == 9)
                        cost += onecost / 2;          // LIN dimensions are for distinguishing between finer exercises, do not let them carry too much weight
                    else
                        cost += onecost;
                }
            } else {
                double znormalize = 0;
                if (dimen == 0 || dimen == 1 || dimen == 2 || dimen == 6 || dimen == 7 || dimen == 8 || dimen == 9)
                    znormalize = (y[dimen][yb % BUFFER_SIZE] - x[dimen][xb % BUFFER_SIZE] + y[dimen][ye % BUFFER_SIZE] - x[dimen][xe % BUFFER_SIZE]) / 2;
                
                float idealPos = (float)(j-1) * (float)(yl-1) / (float)(xl-1);
                int idealPosInt = (int) floor(idealPos);
                float backPortion = idealPos - idealPosInt;
                float ysample = y[dimen][(yb+idealPosInt)%BUFFER_SIZE] * (1-backPortion);
                if (yb+idealPosInt+1<=ye)
                    ysample += y[dimen][(yb+idealPosInt+1)%BUFFER_SIZE] * backPortion;

                cost = fabs(x[dimen][(i - 1 + xb) % BUFFER_SIZE] - ysample + znormalize);
            }
            // recursive compute
            distance[i][j] = cost + MIN(distance[i - 1][j],    // insertion
                                             MIN(distance[i][j - 1],    // deletion
                                                      distance[i - 1][j - 1]));    // match
            //           Log.d(TAG, i + " " + j + " " + distance[i][j]);
        }
    }
    
    // return average distance, longer sequence may be at a disadvantage
    float score = (float) (distance[xl][xl]) / (xl + xl);
    return score;
}


@end

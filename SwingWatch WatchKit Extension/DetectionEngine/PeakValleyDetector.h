//
//  PeakValleyDetector.h
//  vimofit
//
//  Created by Huan Liu on 7/2/15.
//  Copyright © 2015 Vimo Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const int NUM_SAVED_PV;

@interface PeakValleyDetector : NSObject

@property float threshold;  // this is the static threshold
@property float thresholdPercent;  // half of max range in the last few seconds
@property float valleyVal, peakVal;
@property int valleyPos, peakPos;
@property Float64 valleyTimestamp, peakTimestamp;
@property BOOL mTentative;
@property BOOL lpf;

@property float lastsample;  // make public, so that we can log derived samples (accumulation or double-accumulation)
@property int lastindex;
@property BOOL mDynamicThreshold;  // no dynamic threshold at beginning, because we are still looking for a sequence
@property BOOL mDynamicThresholdNoLimit;
@property BOOL mThresholdTimeout;
@property int mMRInterval; // The mean removal interval ideally should be 1 second long, adjust this based on sampling rate
// used for accumulation
@property double mSum, mSumSum;
// used for free-form detection,

// used for free-form detection,
@property int * mLastNPeakPos, *mLastNValleyPos;
@property double *mLastNPeakVal, *mLastNValleyVal;
@property int * mLastNBigPeakPos, *mLastNBigValleyPos;
@property double *mLastNBigPeakVal, *mLastNBigValleyVal;

enum {
    NOPV = 0,
    VALLEY = 1,
    PEAK = 2,
};

- (id) initWithLowPassFilter: (BOOL) lpf;
- (int) postNewSample:(float) sample;
- (int) postNewSample:(float) sample at:(long) timestamp;
- (int) postNewDSample:(float) sample at:(long) timestamp;
- (int) postNewDDSample:(float) sample at:(long) timestamp;
    
- (void) reset;    // reset key values to their initial 
//- (int) noMoreSamples;
- (BOOL) findLastTwoBigPVs:(int) pv;

@end

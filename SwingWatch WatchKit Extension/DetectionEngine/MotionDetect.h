//
//  MotionDetect.h
//  vimofit
//
//  Created by Huan Liu on 7/4/15.
//  Copyright © 2015 Vimo Lab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>

extern const int BUFFER_SIZE;

@protocol MotionDetectDelegate

-(void) setDetecting;
-(void) reportRep:(int)reps wDetector:(int) detector;
-(void) reportDetectedExercise:(NSString *)name;
-(void) showExerciseLogged;   // tell user we have recorded an exercise. This function should vibrate, then bring up edit interface
-(void) logExercise; // log the exercise in SaveState to server


// for logging
-(void) addLog:(NSString *) log;

@end




@interface LogState : NSObject

@property NSString * mOriginalBestMatchedExercise;
@property int mOriginalReps;      // what is the rep before user made any modification

@property NSString * mBestMatchedExercise;

@property NSArray<NSDictionary<NSString *, id> *> * mExercises;
@property int mWeight, mDetector;
@property float ** mWaveform;
@property int mWaveformX, mWaveformY;  // dimensions of the mWaveform array
@property long mStartTime, mLength;   // length is in milliseconds
@property float mVelocity;
@property int mConsistency;

@end





@interface MotionDetect : NSObject

- (void) postNewSensor:(CMDeviceMotion *) deviceMotion;

+ (float **) allocFloatArrayX:(int) sx Y:(int) sy;
+ (void) deallocFloatArray:(float **) array;

- (LogState *) getLogDetails;
- (void) saved:(LogState *) logState atReps:(int)savedReps atWeight:(int)savedWeight atUnit:(int) savedWeightUnit;

- (NSArray<NSDictionary<NSString *, id> *> *) getOrderedList;          // get list based on how well a exercise from library matches the motion. Also include last used weight
- (NSArray<NSDictionary<NSString *, id> *> *) getListOrderedByRecency;  // return a list of dictionary based on last entry time
- (void) reset;    // clear all reps, start over

- (void) logSensorBuffer;

@property NSMutableDictionary * motionLibrary;

+ (id)sharedInstance;


@property (nonatomic, weak) id<MotionDetectDelegate> delegate;

@end

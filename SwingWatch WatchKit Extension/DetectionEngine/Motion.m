//
//  Motion.m
//  vimofit
//
//  Created by Huan Liu on 8/11/15.
//  Copyright © 2015 Vimo Lab. All rights reserved.
//

#import "Motion.h"
#import "MotionDetect.h"

const int NUM_SAVED_MOTION = 3;

@implementation Motion

- (void) reset {
    self.wavePtr = 0;
    self.mBuffer = malloc(NUM_SAVED_MOTION*sizeof(float**));
    self.mDetector = malloc(NUM_SAVED_MOTION*sizeof(int));
    self.mBufferX = malloc(NUM_SAVED_MOTION*sizeof(int));
    self.mBufferY = malloc(NUM_SAVED_MOTION*sizeof(int));
    for (int i=0 ; i<NUM_SAVED_MOTION ; i++) {
        self.mBuffer[i] = nil;
        self.mDetector[i] = -1;
    }
}

- (id) init {
    self = [super init];
    if (self) {
        [self reset];  // initialize data structure
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeInt:self.wavePtr forKey:@"wavePtr"];
    [coder encodeObject:self.mName forKey:@"mName"];
    [coder encodeInt:self.mLastWeight forKey:@"mLastWeight"];
    [coder encodeInt:self.mLastWeightUnit forKey:@"mLastWeightUnit"];
    [coder encodeInt64:self.mLastRecordTime forKey:@"mLastRecordTime"];
    [coder encodeInt:self.mLastReps forKey:@"mLastReps"];
    [coder encodeObject:[NSData dataWithBytes:(void*)self.mDetector length:NUM_SAVED_MOTION*sizeof(int)] forKey:@"mDetector"];
    for (int i=0 ; i<NUM_SAVED_MOTION ; i++) {
        if (self.mBuffer[i]) {
            [coder encodeInt:self.mBufferX[i] forKey:[NSString stringWithFormat:@"mBuffer%ix", i]];
            [coder encodeInt:self.mBufferY[i] forKey:[NSString stringWithFormat:@"mBuffer%iy", i]];
            [coder encodeObject:[NSData dataWithBytes:(void*)self.mBuffer[i][0] length:self.mBufferX[i]*self.mBufferY[i]*  sizeof(float)] forKey:[NSString stringWithFormat:@"mBuffer%i", i]];
        }
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    if (self = [super init]) {
        [self reset];  // initialize data structure

        // read past data
        self.wavePtr = [coder decodeIntForKey:@"wavePtr"];
        self.mName = [coder decodeObjectForKey:@"mName"];
        self.mLastWeight = [coder decodeIntForKey:@"mLastWeight"];
        self.mLastWeightUnit = [coder decodeIntForKey:@"mLastWeightUnit"];
        self.mLastRecordTime = [coder decodeInt64ForKey:@"mLastRecordTime"];
        self.mLastReps = [coder decodeIntForKey:@"mLastReps"];

        NSData *data = [coder decodeObjectForKey:@"mDetector"];
        int *temporary = (int*)[data bytes];
        for(int i = 0; i < NUM_SAVED_MOTION; ++i)
            self.mDetector[i] = temporary[i];
        
        for(int i = 0; i < NUM_SAVED_MOTION; ++i) {
            data = [coder decodeObjectForKey:[NSString stringWithFormat:@"mBuffer%i", i] ];
            if (data) {
                self.mBufferX[i] = [coder decodeIntForKey:[NSString stringWithFormat:@"mBuffer%ix", i] ];
                self.mBufferY[i] = [coder decodeIntForKey:[NSString stringWithFormat:@"mBuffer%iy", i] ];
                self.mBuffer[i] = [MotionDetect allocFloatArrayX:self.mBufferX[i] Y:self.mBufferY[i]];
                float * ftemporary = (float *)[data bytes];

                for (int j=0 ; j<self.mBufferX[i] ; j++)
                    for (int k=0 ; k<self.mBufferY[i] ; k++)
                        self.mBuffer[i][j][k] = ftemporary[j*self.mBufferY[i]+k];
            }
        }
    }
    return self;
}



- (void) dealloc {
    for (int i=0 ; i<NUM_SAVED_MOTION ; i++) {
        [MotionDetect deallocFloatArray:self.mBuffer[i]];
    }

    free(self.mBuffer);
    free(self.mDetector);
    free(self.mBufferX);
    free(self.mBufferY);
}

@end

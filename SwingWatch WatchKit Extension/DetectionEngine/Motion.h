//
//  Motion.h
//  vimofit
//
//  Created by Huan Liu on 8/11/15.
//  Copyright © 2015 Vimo Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const int NUM_SAVED_MOTION;


@interface Motion : NSObject<NSCoding>

@property float*** mBuffer;  // the raw signal
@property int* mBufferX, *mBufferY;   // dimensions of mBuffer array
@property int wavePtr;    // which mBuffer we are writing to next
@property (strong, nonatomic) NSString * mName;   // exercise name
@property int mLastWeight;  // weight used before
@property int mLastWeightUnit;
@property int* mDetector; // which detector found this snippet, other detectors may be good too, but peak/valley is reversed

// these options are for routines with no motion. Sort them by when it was last used
@property long long mLastRecordTime;
@property int mLastReps;

@end

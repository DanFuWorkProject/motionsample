//
//  MeanRemoval.m
//  vimofit
//
//  Created by Huan Liu on 7/3/15.
//  Copyright © 2015 Vimo Lab. All rights reserved.
//

#import "MeanRemoval.h"

@interface MeanRemoval() {
    int mSize;
    int vindex;
    float vsum;
    float * pastv;
    int currentSize;
}
@end

@implementation MeanRemoval

- (id) initWSize:(int)size {
    self = [super init];
    if (self) {
        mSize = size;
        pastv = (float *)malloc(size*sizeof(float));
        for (int i=0;i<size;i++)
            pastv[i] = 0;
    }
    return self;
}

- (float) removeMean:(float) sample {
    vsum -= pastv[vindex];   // remove oldest sample from ring
    pastv[vindex] = sample;
    vsum += sample;
    // advance pointer in ring fashion
    vindex = (vindex+1)%mSize;
    
    // accurately count size at the beginning
    currentSize ++ ;
    if (currentSize>mSize)
        currentSize = mSize;
    
    return sample - vsum/currentSize;
}

- (void) dealloc {
    free(pastv);
}

@end

//
//  PeakValleyDetector.m
//  vimofit
//
//  Created by Huan Liu on 7/2/15.
//  Copyright © 2015 Vimo Lab. All rights reserved.
//

#import "PeakValleyDetector.h"
#import "MeanRemoval.h"
#import "MotionDetect.h"

const int NUM_SAVED_PV=10;
const int LOOKING_FOR_PEAK=1;
const int LOOKING_FOR_VALLEY=2;

@interface PeakValleyDetector() {
    float min, max;
    int minpos, maxpos, learning;
    long minTimestamp, maxTimestamp, lastTimestamp;
    BOOL first; // getting the first sample?
    
    
    float dynThres;  // dynamic threshold, adjusted by the values we have seen
    
    float maxhistory, minhistory;
    
    // used for double integral data to remove drift
    MeanRemoval * aRemoval;
}
@end

@implementation PeakValleyDetector

- (void) reset {
    min = 10000;
    max = -10000;
    minpos = 0;
    maxpos = 0;
    learning = 0;  // learning=1 is going up, learning=2 is going down
    self.peakVal = -1;
    self.valleyVal = -1;
    self.lastindex = 0;
    first = true;
    
    self.valleyPos=-1;
    self.peakPos=-1;
    aRemoval = nil;
    self.mSum = 0;

    for (int i=0 ; i<NUM_SAVED_PV ; i++) {
        self.mLastNPeakPos[i] = -1;
        self.mLastNValleyPos[i] = -1;
        self.mLastNPeakVal[i] = -1;
        self.mLastNValleyVal[i] = -1;
    }

    for (int i=0 ; i<2 ; i++) {
        self.mLastNBigPeakPos[i] = 0;
        self.mLastNBigValleyPos[i] = 0;
        self.mLastNBigPeakVal[i] = 0;
        self.mLastNBigValleyVal[i] = 0;
    }
    
    self.mDynamicThreshold = false;  // no dynamic threshold at beginning, because we are still looking for a sequence
    self.mThresholdTimeout = true;
}

- (BOOL) findLastTwoBigPVs:(int) pv {
    double range = self.peakVal - self.valleyVal;
    int i, j;
    self.mLastNBigPeakPos[1] = -1; self.mLastNBigValleyPos[1] = -1;
    for (i=0 ; i<NUM_SAVED_PV ; i++) {
        double newrange = self.mLastNPeakVal[i] - self.mLastNValleyVal[i];
        if (newrange > self.thresholdPercent * range) {
            self.mLastNBigPeakPos[0] = self.mLastNPeakPos[i];
            self.mLastNBigPeakVal[0] = self.mLastNPeakVal[i];
            self.mLastNBigValleyPos[0] = self.mLastNValleyPos[i];
            self.mLastNBigValleyVal[0] = self.mLastNValleyVal[i];
            range = newrange;
            break;
        }
    }
    for ( j=i+1 ; j<NUM_SAVED_PV ; j++) {
        double newrange = self.mLastNPeakVal[j] - self.mLastNValleyVal[j];
        if (newrange > self.thresholdPercent * range) {
            self.mLastNBigPeakPos[1] = self.mLastNPeakPos[j];
            self.mLastNBigPeakVal[1] = self.mLastNPeakVal[j];
            self.mLastNBigValleyPos[1] = self.mLastNValleyPos[j];
            self.mLastNBigValleyVal[1] = self.mLastNValleyVal[j];
            break;
        }
    }
    if (self.mLastNBigPeakPos[1] == -1 || self.mLastNBigValleyPos[1] == -1)
        return false;    // did not find two past PV with similar range
    if (i == 0 || j == 1)
        return false;    // no different from the normal match
    if (self.lastindex-self.mLastNBigPeakPos[1]>BUFFER_SIZE ||
        self.lastindex-self.mLastNBigValleyPos[1]>BUFFER_SIZE)  // overflow the buffer
        return false;
    
    return true;
}

- (id) initWithLowPassFilter: (BOOL) lpFilter {
    self = [super init];
    if (self) {
        self.lpf = lpFilter;
        
        self.threshold = 0.5f;
        self.thresholdPercent = 0.5f;
        self.mMRInterval = 30;
        
        self.mLastNPeakPos = malloc(NUM_SAVED_PV * sizeof(int));
        self.mLastNValleyPos = malloc(NUM_SAVED_PV * sizeof(int));
        self.mLastNPeakVal = malloc(NUM_SAVED_PV * sizeof(double));
        self.mLastNValleyVal = malloc(NUM_SAVED_PV * sizeof(double));
        self.mLastNBigPeakPos = malloc(2 * sizeof(int));
        self.mLastNBigValleyPos = malloc(2 * sizeof(int));
        self.mLastNBigPeakVal = malloc(2 * sizeof(double));
        self.mLastNBigValleyVal = malloc(2 * sizeof(double));
        
        [self reset];
    }
    return self;
}


- (int) postNewSample:(float) sample {
    return [self postNewSample:sample at:0];
}

// if timestamp is 0, we detect based on indexes, otherwise, record peak/valley 's timestamp
- (int) postNewSample:(float) sample at:(long) timestamp {
    int found = 0;   // 0 = no peak or valley found
    
    if (self.lpf) {
        if (!first)  // Take the first sample as full value, other samples are low pass filtered
            sample = self.lastsample*0.9 + sample*0.1;
    }
    first = false;     // no longer the first sample
    
    self.lastindex ++ ;
    
    if ( sample < min ) {
        min = sample;
        minpos = self.lastindex;
        minTimestamp = timestamp;
    }
    if ( sample > max ) {
        max = sample;
        maxpos = self.lastindex;
        maxTimestamp = timestamp;
    }
    
    
    if (self.mDynamicThreshold) {
        // Dynamic threshold is only enabled when a sequence is identified. Use the last rep's range as threshold so that the current rep (unconfirmed) will not tint the range
        // backtracing also uses dynamic threshold
        if (self.mTentative && learning==LOOKING_FOR_PEAK)
            dynThres = self.thresholdPercent * (self.mLastNPeakVal[0] - self.valleyVal);
        else if (self.mTentative && learning==LOOKING_FOR_VALLEY)
            dynThres = self.thresholdPercent * (self.peakVal - self.mLastNValleyVal[0]);
        else    // !mTentative.  when mTentative, must be looking for either peak or valley
            dynThres = self.thresholdPercent * (self.peakVal - self.valleyVal);
        
        // should the threshold be reset after timeout?
        if ( self.mThresholdTimeout &&
            // bandgoodmorning-12o12-15d last rep would falsely detect if timeout too quickly. Could set a longer timeout, because we walk back anyway
            ((learning == LOOKING_FOR_PEAK && (self.lastindex-self.valleyPos) > 3*(self.valleyPos-self.mLastNValleyPos[0])) ||
             (learning == LOOKING_FOR_VALLEY && (self.lastindex-self.peakPos) > 3*(self.peakPos-self.mLastNPeakPos[0])) ) )
            dynThres = self.threshold;
        
        // guard against too small a range of vibration
        if (!self.mDynamicThresholdNoLimit && dynThres < self.threshold)
            dynThres = self.threshold;
    }
    else
        dynThres = self.threshold;
    
    // learning == 1 means looking for peak
    // learning == 2 means looking for valley
    if ( learning != LOOKING_FOR_PEAK && sample-min > dynThres ) {
        // publicize last valley
        found = [self recordValley];    // 1 = valley found
        self.mTentative = false;
        
        // start recording up. Valley found, looking for Peak
        learning = LOOKING_FOR_PEAK;
        max = sample;
        maxpos = self.lastindex;
        maxTimestamp = timestamp;
    }
    else if (learning == LOOKING_FOR_VALLEY && self.lastsample == min && sample > self.lastsample) {    // looking for valley, has early sign that a valley is found
        //                (learning == 2 && sample == min && lastindex-minpos>10)
        found = [self recordValley];
        self.mTentative = true;
    }

    if ( learning != LOOKING_FOR_VALLEY && max-sample > dynThres ) {
        found = [self recordPeak];   // 2 = peak found
        self.mTentative = false;

        // start recording down
        learning = LOOKING_FOR_VALLEY;
        min = sample;
        minpos = self.lastindex;
        minTimestamp = timestamp;
    }
    else if (learning == LOOKING_FOR_PEAK && self.lastsample == max && sample < self.lastsample) {   // looking for peak, has early sign that a peak is found
        //                (learning == 1 && sample == max && lastindex-minpos>10)
        found = [self recordPeak];
        self.mTentative = true;
    }
    
    // save in case this is the last sample, or for low pass filter
    self.lastsample = sample;
    lastTimestamp = timestamp;  // record both timestamp and index
    
    return found;
}

- (int) recordPeak {
    if (!self.mTentative) {  // last one was not tentative, so we have to shift the peaks
        for (int i=NUM_SAVED_PV-1 ; i>=1 ; i--) {
            self.mLastNPeakPos[i] = self.mLastNPeakPos[i - 1];
            self.mLastNPeakVal[i] = self.mLastNPeakVal[i - 1];
        }
        self.mLastNPeakPos[0] = self.peakPos;
        self.mLastNPeakVal[0] = self.peakVal;
    }
    // record actual value of the peaks
    self.peakVal = max;
    
    self.peakPos = maxpos;
    self.peakTimestamp = maxTimestamp;
    
    // publicize last peak
    return PEAK;   // 2 = peak found
}

- (int) recordValley {
    if (!self.mTentative) {  // last one was not tentative, so we have to shift the valleys
        for (int i=NUM_SAVED_PV-1 ; i>=1 ; i--) {
            self.mLastNValleyPos[i] = self.mLastNValleyPos[i - 1];
            self.mLastNValleyVal[i] = self.mLastNValleyVal[i - 1];
        }
        self.mLastNValleyPos[0] = self.valleyPos;
        self.mLastNValleyVal[0] = self.valleyVal;
    }
    // actual value of the valleys
    self.valleyVal = min;
    
    self.valleyPos = minpos;
    self.valleyTimestamp = minTimestamp;
    
    // publicize last valley
    return VALLEY;    // 1 = valley found
}

- (int) postNewDSample:(float) sample at:(long) timestamp {
    self.mSum += sample;
    
    return [self postNewSample:self.mSum at:timestamp];
}

- (int) postNewDDSample:(float) sample at:(long) timestamp {
    if (aRemoval==nil)
        aRemoval = [[MeanRemoval alloc] initWSize:self.mMRInterval];
    
    double acc = [aRemoval removeMean:sample]; // critical to remove mean first, sometimes there is bias, hard to correct later
    self.mSum += acc;  // speed
    
    return [self postNewSample:self.mSum at:timestamp];
}

- (void) dealloc {
    free(self.mLastNPeakPos);
    free(self.mLastNValleyPos);
    free(self.mLastNPeakVal);
    free(self.mLastNValleyVal);
    free(self.mLastNBigPeakPos);
    free(self.mLastNBigValleyPos);
    free(self.mLastNBigPeakVal);
    free(self.mLastNBigValleyVal);
}


// call at the end to detect if there is any more peak or valley
// TODO, no timestamp approach for this call, but for now, HeartRate does not use this, so we are ok
//- (int) noMoreSamples {
//    int found = 0;
//    if ( learning == 1 && lastsample-min > self.threshold ) {  // last sample is a peak
//        found = PEAK;   // 2 = peak found
//        self.peak = lastsample;
//        self.peakPos = lastindex;
//    }
//    if ( learning == 2 && max-lastsample > self.threshold ) { // last sample is a valley
//        found = VALLEY;    // 1 = valley found
//        self.valley = lastsample;
//        self.valleyPos = lastindex;
//    }
//    return found;
//}

@end

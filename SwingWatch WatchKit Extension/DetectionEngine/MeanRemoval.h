//
//  MeanRemoval.h
//  vimofit
//
//  Created by Huan Liu on 7/3/15.
//  Copyright © 2015 Vimo Lab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MeanRemoval : NSObject

- (id) initWSize:(int)size;
- (float) removeMean:(float) sample;

@end
